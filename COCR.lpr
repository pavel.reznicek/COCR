program COCR;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, COCRMain, betanetwork, COCRUtils,
  runtimetypeinfocontrols;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfCOCR, fCOCR);
  Application.Run;
end.

