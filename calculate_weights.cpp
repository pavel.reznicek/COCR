public void calculate_weights()
{
    for(int l=1;l<number_of_layers;l++)
        for(int i=0;i<layers[l];i++)
            for(int j=0;j<layers[l-1];j++)
            {
                weight[l,i,j] = (float)(weight[l,i,j] + 
                    learning_rate*error[l,i]*node_output[l-1,j]);
            }
}
