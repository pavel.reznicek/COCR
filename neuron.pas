unit neuron;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, ExtCtrls, Variants, fgl, Laz2_DOM;
  
const DefaultSigmoidSlope = {0.014} 0.5;

var SigmoidSlope: Extended;

function Sigmoid(Net: Extended): Extended;
function SigmoidDerivative(SigmoidResult: Extended): Extended;

type
  TActivationFunction = record
    Func: function(Net: Extended): Extended;
    Derivative: function(AFResult: Extended): Extended;
  end;

  TNeuron = class;

  TNeuronKind = (
    nkInput,
    nkHidden,
    nkOutput,
    nkInputBias,
    nkHiddenBias
  );

  { TSynapse }

  TSynapse = class
  private
    FInputNeuron: TNeuron;
    FOutputNeuron: TNeuron;
    FWeight: Extended;
    FAdjustedWeight: Extended;
    FOnChange: TNotifyEvent;
    FOnInnerChange: TNotifyEvent;
    procedure SetWeight(AValue: Extended);
  protected
    property OnInnerChange: TNotifyEvent read FOnInnerChange
      write FOnInnerChange; { used to inform neurons in the next layer about
        changes; we need a separate event for that; declared as protected to be
        available in this unit only }
  public
    procedure SaveYourself(Doc: TXMLDocument; ParentNeuronElement: TDOMElement);
    procedure ApplyAdjustedWeight;
  published
    property InputNeuron: TNeuron read FInputNeuron write FInputNeuron;
    property OutputNeuron: TNeuron read FOutputNeuron write FOutputNeuron;
    property Weight: Extended read FWeight write SetWeight;
    property AdjustedWeight: Extended read FAdjustedWeight
      write FAdjustedWeight;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

  { TSynapseList }

  TSynapseList = specialize TFPGObjectList<TSynapse>;

  {TNeuronChange = procedure(Sender: TObject; Syn: TSynapse) of object;
  PNeuronChange = ^TNeuronChange;}

  ENeuronError = class(Exception);

  { TNeuron }

  {$Define NEURON_DYNAMIC_VALUE} { A flag meaning that the neuron is computed
    dynamically on request or statically from the outside }
  TNeuron = class(TObject)
  private
    FLayerIndex: Integer;
    FNeuronIndex: Integer;
    FBackwardSynapseList: TSynapseList;
    FForwardSynapseList: TSynapseList;
    FOnChange: TNotifyEvent;
    FKind: TNeuronKind;
    FError: Extended;
    {$IFNDEF NEURON_DYNAMIC_VALUE}
    FValue: Extended;
    {$ENDIF}
    FActivationFunction: TActivationFunction;
    function GetBackwardSynapse(Index: Integer): TSynapse;
    function GetForwardSynapse(Index: Integer): TSynapse;
    function GetBackwardSynapseCount: Integer;
    function GetForwardSynapseCount: Integer;
    procedure SetBackwardSynapse(Index: Integer; NewSynapse: TSynapse);
    procedure SetForwardSynapse(Index: Integer; NewSynapse: TSynapse);
    function GetValue: Extended;
    {$IFNDEF NEURON_DYNAMIC_VALUE}
    procedure SetValue(AValue: Extended);
    {$ENDIF}
    procedure BackwardSynapseInnerChange(Sender: TObject);
  public
    constructor Create;
    destructor Destroy; override;
    function AddBackwardSynapse(N: TNeuron): TSynapse;
    function AddForwardSynapse(N: TNeuron): TSynapse;
    function AddInput: TSynapse;
    procedure Evaluate(Error: Extended);
    function GetMinWeight: Extended;
    function GetMaxWeight: Extended;
    function CalculateActivation(Net: Extended): Extended;
    function CalculateDerivative(Activation: Extended): Extended;
    function CalculateDerivative: Extended;
    procedure ApplyAdjustedWeights;
    procedure Update;
    procedure SaveYourself(Doc: TXMLDocument; LayerElement: TDOMElement);
    property BackwardSynapseList: TSynapseList read FBackwardSynapseList
      write FBackwardSynapseList;
    property ForwardSynapseList: TSynapseList read FForwardSynapseList
      write FForwardSynapseList;
    property BackwardSynapses[Index: Integer]: TSynapse read GetBackwardSynapse
      write SetBackwardSynapse;
    property ForwardSynapses[Index: Integer]: TSynapse read GetForwardSynapse
      write SetForwardSynapse;
    property BackwardSynapseCount: Integer read GetBackwardSynapseCount;
    property ForwardSynapseCount: Integer read GetForwardSynapseCount;
    property Value: Extended read GetValue
      {$IFNDEF NEURON_DYNAMIC_VALUE} write SetValue {$ENDIF};
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property Kind: TNeuronKind read FKind;
    property Error: Extended read FError write FError;
    property NeuronIndex: Integer read FNeuronIndex write FNeuronIndex;
    property LayerIndex: Integer read FLayerIndex write FLayerIndex;
    property ActivationFunction: TActivationFunction read FActivationFunction
      write FActivationFunction;
  end;

  { TInputNeuron }

  TInputNeuron = class(TNeuron)
  private
    function GetInput: Extended;
    procedure SetInput(AValue: Extended);
  public
    constructor Create;
  published
    property Input: Extended read GetInput write SetInput;
  end;

  { TInputBias }

  TInputBias = class(TInputNeuron)
  private
    function GetInput: Extended;
  public
    constructor Create;
  published
    property Input: Extended read GetInput;
  end;

  { THiddenNeuron }

  THiddenNeuron = class(TNeuron)
    constructor Create;
  end;

  { THiddenBias }

  THiddenBias = class(THiddenNeuron)
  private
    function GetInput: Extended;
    procedure SetInput(AValue: Extended);
  public
    constructor Create;
  published
    property Input: Extended read GetInput;
  end;

  { TOutputNeuron }

  TOutputNeuron = class(TNeuron)
    constructor Create;
  end;

const
  SigmoidActivationFuction: TActivationFunction = (
    Func: @Sigmoid;
    Derivative: @SigmoidDerivative
  );

function CreateNeuronOfKind(Kind: TNeuronKind): TNeuron;

implementation

function Sigmoid(Net: Extended): Extended;
begin
  // float result=(float)((2/(1+Math.Exp(-1*slope*f_net)))-1); // Bipolar
  Result := (2 / (1 + exp(-1 * SigmoidSlope * Net))) - 1;
end;

function SigmoidDerivative(SigmoidResult: Extended): Extended;
begin
  // float derivative=(float)(0.5F*(1-Math.Pow(result,2))); // Bipolar
  Result := 0.5 * (1 - sqr(SigmoidResult));
end;

function CreateNeuronOfKind(Kind: TNeuronKind): TNeuron;
begin
  case Kind of
  nkInput:
    Result := TInputNeuron.Create;
  nkHidden:
    Result := THiddenNeuron.Create;
  nkOutput:
    Result := TOutputNeuron.Create;
  nkInputBias:
    Result := TInputBias.Create;
  nkHiddenBias:
    Result := THiddenBias.Create;
  else
    Result := TInputNeuron.Create;
    raise ENeuronError.Create('Wrong neuron kind passed to the ' +
      'CreateNeuronOfKind function!');
  end;
end;

{ THiddenBias }

function THiddenBias.GetInput: Extended;
var
  InputSynapse: TSynapse;
begin
  if BackwardSynapseCount = 1 then
  begin
    InputSynapse:=BackwardSynapses[0];
    if InputSynapse.InputNeuron = nil then
      Result:=InputSynapse.Weight
    else
    begin
      Result:=0;
      raise ENeuronError.Create('An input synapse''s neuron has to be empty ' +
        '(nil).');
    end;
  end
  else
  begin
    Result:=0;
    raise ENeuronError.Create('A bias neuron''s synapse array has to have ' +
      'exactly one input synapse.');
  end;
end;

procedure THiddenBias.SetInput(AValue: Extended);
var
  InputSynapse: TSynapse;
begin
  if BackwardSynapseCount = 1 then
  begin
    InputSynapse := BackwardSynapses[0];
    if InputSynapse.InputNeuron = nil then
      InputSynapse.Weight:=AValue
    else
      raise ENeuronError.Create('An input synapse''s neuron has to be empty ' +
        '(nil).');
  end
  else
    raise ENeuronError.Create('A bias neuron''s synapse list has to have ' +
      'exactly one input synapse.');
end;

constructor THiddenBias.Create;
begin
  inherited Create;
  AddInput;
  SetInput(1);
  FKind:=nkHiddenBias;
  FActivationFunction.Func := nil;
  FActivationFunction.Derivative := nil;
end;

{ TInputBias }

function TInputBias.GetInput: Extended;
begin
  Result := inherited GetInput;
end;

constructor TInputBias.Create;
begin
  inherited Create;
  FKind:=nkInputBias;
  inherited SetInput(1);
end;

{ TSynapse }

procedure TSynapse.SetWeight(AValue: Extended);
begin
  if FWeight=AValue then Exit;
  FWeight:=AValue;
  if Assigned(FOnChange) then
    FOnChange(Self);
  if Assigned(FOnInnerChange) then
    FOnInnerChange(Self);
end;

procedure TSynapse.SaveYourself(Doc: TXMLDocument;
  ParentNeuronElement: TDOMElement);
var
  SynapseElement: TDOMElement;
  FS: TFormatSettings;
begin
  SynapseElement := Doc.CreateElement('Synapse');
  if FInputNeuron <> nil then
  begin
    SynapseElement['LayerIndex'] := IntToStr(FInputNeuron.LayerIndex);
    SynapseElement['NeuronIndex'] := IntToStr(FInputNeuron.NeuronIndex);
  end;
  FS := DefaultFormatSettings;
  FS.DecimalSeparator:='.';
  SynapseElement['Weight'] := FloatToStr(FWeight, FS);
  ParentNeuronElement.AppendChild(SynapseElement);
end;

procedure TSynapse.ApplyAdjustedWeight;
begin
  if InputNeuron <> nil then
    SetWeight(FAdjustedWeight);
end;

//uses Math;

{ TOutputNeuron }

constructor TOutputNeuron.Create;
begin
  inherited Create;
  FKind:=nkOutput;
  FActivationFunction := SigmoidActivationFuction;
end;

{ THiddenNeuron }

constructor THiddenNeuron.Create;
begin
  inherited Create;
  FKind:=nkHidden;
  FActivationFunction := SigmoidActivationFuction;
end;

{ TInputNeuron }

function TInputNeuron.GetInput: Extended;
var
  InputSynapse: TSynapse;
begin
  if BackwardSynapseCount = 1 then
  begin
    InputSynapse:=BackwardSynapses[0];
    if InputSynapse.InputNeuron = nil then
      Result:=InputSynapse.Weight
    else
    begin
      Result:=0;
      raise ENeuronError.Create('An input synapse''s neuron has to be empty ' +
        '(nil).');
    end;
  end
  else
  begin
    Result:=0;
    raise ENeuronError.Create('An input neuron''s synapse list has to ' +
      'contain exactly one input synapse.');
  end;
end;

procedure TInputNeuron.SetInput(AValue: Extended);
var
  InputSynapse: TSynapse;
begin
  if BackwardSynapseCount = 1 then
  begin
    InputSynapse := BackwardSynapses[0];
    if InputSynapse.InputNeuron = nil then
      InputSynapse.Weight:=AValue
    else
      raise ENeuronError.Create('An input synapse''s neuron has to be empty ' +
        '(nil).');
  end
  else
    raise ENeuronError.Create('An input neuron''s synapse list has to ' +
      'contain exactly one input synapse.');
end;

constructor TInputNeuron.Create;
begin
  inherited Create;
  FKind:=nkInput;
  AddInput;
end;

{ TNeuron }

{procedure TNeuron.MouseMove(Shift: TShiftState; X, Y: Integer);
begin
  inherited MouseMove(Shift, X, Y);
  Hint := FloatToStr(GetValue);
end;}

function TNeuron.GetBackwardSynapse(Index: Integer): TSynapse;
begin
  Result := FBackwardSynapseList.Items[Index];
end;

function TNeuron.GetForwardSynapse(Index: Integer): TSynapse;
begin
  Result := FForwardSynapseList.Items[Index];
end;

function TNeuron.GetBackwardSynapseCount: Integer;
begin
  if FBackwardSynapseList <> nil then
    Result := FBackwardSynapseList.Count
  else
    Result := 0;
end;

function TNeuron.GetForwardSynapseCount: Integer;
begin
  Result := FForwardSynapseList.Count;
end;

procedure TNeuron.SetBackwardSynapse(Index: Integer; NewSynapse: TSynapse);
begin
  FBackwardSynapseList.Items[Index] := NewSynapse;
end;

procedure TNeuron.SetForwardSynapse(Index: Integer; NewSynapse: TSynapse);
begin
  FBackwardSynapseList.Items[Index] := NewSynapse;
end;

{$IfNDef NEURON_DYNAMIC_VALUE}
procedure TNeuron.SetValue(AValue: Extended);
begin
  // Limit input value
  if AValue > 1 then
    AValue := 1
  else if AValue < -1 then
    AValue := -1;
  if FValue = AValue then Exit;
  FValue := AValue;
  if Assigned(FOnChange) then
    FOnChange(Self);
end;
{$EndIf}

procedure TNeuron.BackwardSynapseInnerChange(Sender: TObject);
begin
  {.$IfDef NEURON_DYNAMIC_VALUE}
  if Assigned(FOnChange) then
    FOnChange(Self);
  Update;
  {.$EndIf}
end;

constructor TNeuron.Create;
begin
  inherited Create;
  FBackwardSynapseList := TSynapseList.Create(True);
  FForwardSynapseList := TSynapseList.Create(False);
  FActivationFunction.Func := nil;
  FActivationFunction.Derivative := nil;
end;

destructor TNeuron.Destroy;
begin
  FBackwardSynapseList.Free;
  FForwardSynapseList.Free;
  inherited Destroy;
end;

// Addition of a backward synapse (provides the input for this neuron)
function TNeuron.AddBackwardSynapse(N: TNeuron): TSynapse;
begin
  //SetLength(FSynapseArray, Length(FSynapseArray) + 1);
  //FSynapseArray[High(FSynapseArray)].Neuron := N;
  //FSynapseArray[High(FSynapseArray)].Weight := (Random * 2.0 - 1.0);
  //Result := FSynapseArray[High(FSynapseArray)];
  Result := TSynapse.Create;
  Result.InputNeuron := N;
  Result.OutputNeuron := Self;
  Result.Weight := (Random * 2.0 - 1.0) {* 1000};
  //Result.Weight := 1;
  //Result.Weight := 0;
  Result.OnInnerChange := @BackwardSynapseInnerChange;
  FBackwardSynapseList.Add(Result);
  {if Assigned(FOnChange) then
    FOnChange(Self, Result);}
end;

function TNeuron.AddForwardSynapse(N: TNeuron): TSynapse;
begin
  Result := TSynapse.Create;
  Result.OutputNeuron := N;
  Result.InputNeuron := Self;
  Result.Weight := (Random * 2.0 - 1.0) {* 1000};
  //Result.Weight := 1;
  //Result.Weight := 0;
  FForwardSynapseList.Add(Result);
  {if Assigned(FOnChange) then
    FOnChange(Self, Result);}
end;

// Adds a synapse without a neuron (and its weight then serves as an inputs)
function TNeuron.AddInput: TSynapse;
begin
  Result := AddBackwardSynapse(nil);
end;

function TNeuron.GetValue: Extended;
{$IfDef NEURON_DYNAMIC_VALUE}
var
  Summa, {Average,} W, V, Activation: Extended;
  S: Integer;
  Synapse: TSynapse;
{$EndIf}
begin
  {$IfDef NEURON_DYNAMIC_VALUE}
  Summa := 0.0;
  for S := 0 to FBackwardSynapseList.Count - 1 do
  begin
    Synapse := BackwardSynapses[S];
    if Synapse.InputNeuron <> nil then
    begin
      V := Synapse.InputNeuron.Value;
      W := Synapse.Weight;
    end
    else // nil -> direct input
    begin
      V := 1.0;
      W := Synapse.Weight;
    end;
    Summa := Summa + V * W;
  end;

  if Assigned(FActivationFunction.Func) then
    Activation := FActivationFunction.Func(Summa)
  else
    Activation := Summa;

  if Activation < -1.0 then
    Result := -1.0
  else if Activation > 1.0 then
    Result := 1.0
  else
    Result := Activation;
  {$Else}
  Result := FValue;
  {$EndIf}
end;

procedure TNeuron.Evaluate(Error: Extended);
var
//  MyValue: Extended;
  W, V, E{, C}: Extended;
  S: Integer; // čítač synapsí
  Delta: Extended;
begin
  {
    Var. A
    According to that condition that the product of the output neuron value
    and weight converges to the result given by the teacher, we raise or lower
    the weight (we do emphatisation or penalisation).
  }
  {
    Var. B
    A bit conventionallier: We count the ratio of the desired result
    and the input values average - that will be the new value.
  }
  (*
    Var. C
    According to a howto on the http://www.heatonresearch.com/node/699 page:
    Formula: Δw_{i,j} = 2μx_i(ideal-actual)_j
  *)
  //MyValue:=GetValue;
  //C := Length(SynapseArray);
  for S := 0 to FBackwardSynapseList.Count - 1 do
  begin
    if FBackwardSynapseList[S].InputNeuron <> nil then
    begin
      W := BackwardSynapses[S].Weight;
      V := BackwardSynapses[S].InputNeuron.Value;
      // This is handled by the Value property:
      //if V > 1 then V := 1;
      //if V < -1 then V := -1;
      //E:=V * Error / MyValue;
      // E_i = E_j * W_ij * V_i
      E:=Error {/ C} * W * V;
      Delta := 2 * 1 * V * E;
      {if Delta < 0 then
        Delta := Delta / 256; // Let's make the inhibition slower than exhibition.}
      BackwardSynapses[S].Weight := W + Delta;
      { The cutting of the synapse weights didn't work out -
      - the network learned only limitedly and it couldn't progress further. }
      //if SynapseArray[S].Weight > 1 then SynapseArray[S].Weight := 1;
      //if SynapseArray[S].Weight < -1 then SynapseArray[S].Weight := -1;
    end;
    if BackwardSynapses[S].InputNeuron <> nil then
    begin
      BackwardSynapses[S].InputNeuron.Evaluate(E);
    end;
  end;
end;

function TNeuron.GetMinWeight: Extended;
var S: Integer;
begin
  Result := BackwardSynapses[0].Weight;
  for S := 1 to BackwardSynapseCount - 1 do
  begin
    if BackwardSynapses[S].Weight < Result then
    begin
      Result := BackwardSynapses[S].Weight;
    end;
  end;
end;

function TNeuron.GetMaxWeight: Extended;
var S: Integer;
begin
  Result := BackwardSynapses[0].Weight;
  for S := 1 to BackwardSynapseCount - 1 do
  begin
    if BackwardSynapses[S].Weight > Result then
    begin
      Result := BackwardSynapses[S].Weight;
    end;
  end;
end;

function TNeuron.CalculateActivation(Net: Extended): Extended;
begin
  if FActivationFunction.Func <> nil then
    Result := FActivationFunction.Func(Net)
  else
    Result := Net;
end;

function TNeuron.CalculateDerivative(Activation: Extended): Extended;
begin
  if FActivationFunction.Derivative <> nil then
    Result := FActivationFunction.Derivative(Activation)
  else
    Result := Activation;
end;

function TNeuron.CalculateDerivative: Extended;
begin
  if FActivationFunction.Derivative <> nil then
    Result := FActivationFunction.Derivative(Value)
  else
    Result := Value;
end;

procedure TNeuron.ApplyAdjustedWeights;
var
  S: Integer;
  objS: TSynapse;
begin
  for S := 0 to BackwardSynapseCount - 1 do
  begin
    objS := BackwardSynapses[S];
    objS.ApplyAdjustedWeight;
  end;
end;

procedure TNeuron.Update;
var
  S: Integer;
  objS: TSynapse;
  objN: TNeuron;
begin
  if Assigned(FOnChange) then
    FOnChange(Self);
  for S := 0 to ForwardSynapseCount - 1 do
  begin
    objS := ForwardSynapses[S];
    objN := objS.OutputNeuron;
    objN.Update;
  end;
end;

procedure TNeuron.SaveYourself(Doc: TXMLDocument; LayerElement: TDOMElement);
var
  NeuronElement: TDOMElement;
  S: Integer;
  objS: TSynapse;
begin
  NeuronElement := Doc.CreateElement('Neuron');
  NeuronElement['NeuronIndex'] := IntToStr(NeuronIndex);
  NeuronElement['Kind'] := IntToStr(Integer(FKind));
  for S := 0 to BackwardSynapseCount - 1 do
  begin
    objS := GetBackwardSynapse(S);
    objS.SaveYourself(Doc, NeuronElement);
  end;
  LayerElement.AppendChild(NeuronElement);
end;



initialization

  SigmoidSlope := DefaultSigmoidSlope;
  Randomize;

end.

