unit BetaNetwork;

{$mode objfpc}{$H+}

interface

(*
import random
import math
*)
uses
  Classes, SysUtils, Laz2_DOM, laz2_XMLRead, laz2_XMLWrite;

(*
#
# Shorthand:
#   "pd_" as a variable prefix means "partial derivative"
#   "d_" as a variable prefix means "derivative"
#   "_wrt_" is shorthand for "with respect to"
#   "w_ho" and "w_ih" are the index of weights from hidden to output layer
#      neurons and input to hidden layer neurons respectively
#
# Comment references:
#
# [1] Wikipedia article on Backpropagation
#   http://en.wikipedia.org/wiki/Backpropagation#Finding_the_derivative_of_the_error
# [2] Neural Networks for Machine Learning course on Coursera by Geoffrey Hinton
#   https://class.coursera.org/neuralnets-2012-001/lecture/39
# [3] The Back Propagation Algorithm
#   https://www4.rgu.ac.uk/files/chapter3%20-%20bp.pdf
*)

type
  TNeuralFloat = Extended;
  TNeuralFloatArray = array of TNeuralFloat;

  TTrainingSetPartType = (
    tsptInput = 0,
    tsptOutput = 1
  );

  TTrainingSet = array[TTrainingSetPartType] of TNeuralFloatArray;

  TTrainingSets = array of TTrainingSet;

  ENetworkError = class(Exception);

  TNeuronLayer = class;

  // class NeuralNetwork:
  { TNeuralNetwork }

  TNeuralNetwork = class
  private
    FAbsoluteMatchAchieved: Boolean;
    FModified: Boolean;
    FOnModifiedChange: TNotifyEvent;
    FIterationCount: QWord;
    FEpochCount: QWord;
    FTeachCharsToTeachOnly: Boolean;
    FCharsToTeach: String;
    procedure SetAbsoluteMatchAchieved(AValue: Boolean);
    procedure SetEpochCount(AValue: QWord);
    procedure SetIterationCount(AValue: QWord);
    procedure SetModified(AValue: Boolean);
    procedure SetTeachCharsToTeachOnly(AValue: Boolean);
    procedure SetCharsToTeach(AValue: String);
  public
    // LEARNING_RATE = 0.5
    LEARNING_RATE: TNeuralFloat;
    OUTPUT_THRESHOLD: TNeuralFloat;
    num_inputs: Integer;
    bias: Integer;
    hidden_layer: TNeuronLayer;
    output_layer: TNeuronLayer;
    { def __init__(self, num_inputs, num_hidden, num_outputs,
        hidden_layer_weights = None, hidden_layer_bias = None,
        output_layer_weights = None, output_layer_bias = None): }
    constructor Create(initial_num_inputs, num_hidden, num_outputs: Integer;
      hidden_layer_weights: array of TNeuralFloat;
      hidden_layer_bias: TNeuralFloat = 0;
      output_layer_weights: array of TNeuralFloat;
      output_layer_bias: TNeuralFloat = 0);
    constructor Create(FilePath: String);
    destructor Destroy; override;
    { def init_weights_from_inputs_to_hidden_layer_neurons(self,
        hidden_layer_weights): }
    procedure init_weights_from_inputs_to_hidden_layer_neurons(
      hidden_layer_weights: array of TNeuralFloat);
    { def init_weights_from_hidden_layer_neurons_to_output_layer_neurons(self,
        output_layer_weights): }
    procedure init_weights_from_hidden_layer_neurons_to_output_layer_neurons(
      output_layer_weights: array of TNeuralFloat);
    // def inspect(self):
    procedure inspect();
    // def feed_forward(self, inputs):
    function feed_forward(inputs: TNeuralFloatArray): TNeuralFloatArray;
    // def train(self, training_inputs, training_outputs):
    procedure train(training_inputs, training_outputs: TNeuralFloatArray);
    procedure train(training_set: TTrainingSet);
    // def calculate_total_error(self, training_sets):
    function calculate_total_error(training_sets: TTrainingSets): TNeuralFloat;
    procedure SaveYourself(Path: String);
    procedure EpochDone;
    property HiddenLayer: TNeuronLayer read hidden_layer;
    property OutputLayer: TNeuronLayer read output_layer;
    {property OutputThreshold: TNeuralFloat read OUTPUT_THRESHOLD
      write OUTPUT_THRESHOLD;}
  published
    property LearningRate: TNeuralFloat read LEARNING_RATE write LEARNING_RATE;
    property Modified: Boolean read FModified write SetModified;
    property OnModifiedChange: TNotifyEvent read FOnModifiedChange
      write FOnModifiedChange;
    property IterationCount: QWord read FIterationCount write SetIterationCount;
    property EpochCount: QWord read FEpochCount write SetEpochCount;
    property AbsoluteMatchAchieved: Boolean read FAbsoluteMatchAchieved
      write SetAbsoluteMatchAchieved;
    property CharsToTeach: String read FCharsToTeach write SetCharsToTeach;
    property TeachCharsToTeachOnly: Boolean read FTeachCharsToTeachOnly
      write SetTeachCharsToTeachOnly;
  end;

  TNeuron = class;

  // class NeuronLayer:
  { TNeuronLayer }

  TNeuronLayer = class
  private
    function GetCount: Integer;
    function GetItem(Index: Integer): TNeuron;
  public
    bias: TNeuralFloat;
    neurons: array of TNeuron;
    // def __init__(self, num_neurons, bias):
    constructor Create(num_neurons: Integer; initial_bias: TNeuralFloat);
    destructor Destroy; override;
    // def inspect(self):
    procedure inspect();
    // def feed_forward(self, inputs):
    function feed_forward(inputs: TNeuralFloatArray): TNeuralFloatArray;
    // def get_outputs(self):
    function get_outputs(): TNeuralFloatArray;
    procedure SaveYourself(Doc: TXMLDocument; NetworkElement: TDOMElement);
    property Count: Integer read GetCount;
    property Items[Index: Integer]: TNeuron read GetItem; default;
end;

  // class Neuron:
  { TNeuron }

  TNeuron = class
  public
    bias: TNeuralFloat;
    weights: array of TNeuralFloat;
    inputs: array of TNeuralFloat;
    output: TNeuralFloat;
    // def __init__(self, bias):
    constructor Create(initial_bias: TNeuralFloat);
    // def calculate_output(self, inputs):
    function calculate_output(inputs_: TNeuralFloatArray): TNeuralFloat;
    // def calculate_total_net_input(self):
    function calculate_total_net_input(): TNeuralFloat;
    // def squash(self, total_net_input):
    function squash(total_net_input: TNeuralFloat): TNeuralFloat;
    // def calculate_pd_error_wrt_total_net_input(self, target_output):
    function calculate_pd_error_wrt_total_net_input(
      target_output: TNeuralFloat
    ): TNeuralFloat;
    // def calculate_error(self, target_output):
    function calculate_error(target_output: TNeuralFloat): TNeuralFloat;
    // def calculate_pd_error_wrt_output(self, target_output):
    function calculate_pd_error_wrt_output(target_output: TNeuralFloat):
      TNeuralFloat;
    // def calculate_pd_total_net_input_wrt_input(self):
    function calculate_d_total_net_input_wrt_input(): TNeuralFloat;
    // def calculate_pd_total_net_input_wrt_weight(self, index):
    function calculate_pd_total_net_input_wrt_weight(index: Integer):
      TNeuralFloat;
    function GetMinWeight: TNeuralFloat;
    function GetMaxWeight: TNeuralFloat;
    procedure SaveYourself(Doc: TXMLDocument; LayerElement: TDOMElement);
  end;

const
  DefaultLearningRate = 0.5;
  //DefaultOutputThreshold = 0.5;

  txtLearningRate = 'LearningRate';
  //txtOutputThreshold = 'OutputThreshold';
  txtBias = 'Bias';
  txtNumInputs = 'NumInputs';
  txtIterationCount = 'IterationCount';
  txtEpochCount = 'EpochCount';
  txtAbsoluteMatchAchieved = 'AbsoluteMatchAchieved';
  txtCharsToTeach = 'CharsToTeach';
  txtTeachCharsToTeachOnly = 'TeachCharsToTeachOnly';
  txtLayer = 'Layer';
  txtNeuron = 'Neuron';
  txtWeight = 'Weight';
  txtValue = 'Value';

  function IsEmptyArray(TheArray: array of TNeuralFloat): Boolean;
  procedure AppendNeuralFloat(var TheArray: TNeuralFloatArray; Item: TNeuralFloat);
  function CreateTrainingSet(Inputs: TNeuralFloatArray;
    Outputs: TNeuralFloatArray):
    TTrainingSet;
  function FormatNeuralFloatArray(FormatStr: String;
    NeuralFloatArray: TNeuralFloatArray):
    String;
  function GetOptimalNumberOfHiddenNeurons(NumberOfInputs,
    NumberOfOutputs, NumberOfSamples, ScalingFactor: Integer): Integer;

implementation

uses COCRUtils;

{ TNeuralNetwork }

procedure TNeuralNetwork.SetModified(AValue: Boolean);
begin
  if FModified=AValue then Exit;
  FModified:=AValue;
  if Assigned(FOnModifiedChange) then
    FOnModifiedChange(Self);
end;

procedure TNeuralNetwork.SetTeachCharsToTeachOnly(AValue: Boolean);
begin
  if FTeachCharsToTeachOnly=AValue then Exit;
  FTeachCharsToTeachOnly:=AValue;
end;

procedure TNeuralNetwork.SetCharsToTeach(AValue: String);
begin
  if FCharsToTeach=AValue then Exit;
  FCharsToTeach:=AValue;
end;

procedure TNeuralNetwork.SetEpochCount(AValue: QWord);
begin
  if FEpochCount=AValue then Exit;
  FEpochCount:=AValue;
end;

procedure TNeuralNetwork.SetAbsoluteMatchAchieved(AValue: Boolean);
begin
  if FAbsoluteMatchAchieved=AValue then Exit;
  FAbsoluteMatchAchieved:=AValue;
end;

procedure TNeuralNetwork.SetIterationCount(AValue: QWord);
begin
  if FIterationCount=AValue then Exit;
  FIterationCount:=AValue;
end;

{ def __init__(self, num_inputs, num_hidden, num_outputs,
    hidden_layer_weights = None, hidden_layer_bias = None,
    output_layer_weights = None, output_layer_bias = None): }
constructor TNeuralNetwork.Create(initial_num_inputs, num_hidden,
  num_outputs: Integer; hidden_layer_weights: array of TNeuralFloat;
  hidden_layer_bias: TNeuralFloat; output_layer_weights: array of TNeuralFloat;
  output_layer_bias: TNeuralFloat);
begin
  inherited Create;

  LEARNING_RATE := DefaultLearningRate;
  //OUTPUT_THRESHOLD := DefaultOutputThreshold;
  FIterationCount := 0;
  FEpochCount := 0;
  FAbsoluteMatchAchieved := False;
  FCharsToTeach := '';
  FTeachCharsToTeachOnly := True;

  // self.num_inputs = initial_num_inputs
  Self.num_inputs := initial_num_inputs;

  // self.hidden_layer = NeuronLayer(num_hidden, hidden_layer_bias)
  Self.hidden_layer := TNeuronLayer.Create(num_hidden, hidden_layer_bias);
  //self.output_layer = NeuronLayer(num_outputs, output_layer_bias)
  Self.output_layer := TNeuronLayer.Create(num_outputs, output_layer_bias);

  // self.init_weights_from_inputs_to_hidden_layer_neurons(hidden_layer_weights)
  Self.init_weights_from_inputs_to_hidden_layer_neurons(hidden_layer_weights);
  { self.init_weights_from_hidden_layer_neurons_to_output_layer_neurons(
      output_layer_weights) }
  Self.init_weights_from_hidden_layer_neurons_to_output_layer_neurons(
    output_layer_weights);

end;

constructor TNeuralNetwork.Create(FilePath: String);
var
  FS: TFormatSettings;
  Doc: TXMLDocument;
  NetworkElement: TDOMElement;
  sLearningRate: String;
  flLearningRate: TNeuralFloat;
  //sOutputThreshold: String;
  //sngOutputThreshold: TNeuralFloat;
  sAbsoluteMatchAchieved: String;
  bAbsoluteMatchAchieved: Boolean;
  sCharsToTeach: String;
  sTeachCharsToTeachOnly: String;
  bTeachCharsToTeachOnly: Boolean;
  sNumInputs, sIterationCount, sEpochCount: String;
  iNumInputs, iNumNeurons, iNumHidden, iNumOutputs:  Integer;
  qIterationCount, qEpochCount: QWord;
  LayerElementList: TDOMElementList;
  LayerElement: TDOMElement;
  L, N, W: Integer;
  NeuronElementList: TDOMElementList;
  NeuronElement: TDOMElement;
  sLayerBias: String;
  LayerBias, HiddenLayerBias, OutputLayerBias: TNeuralFloat;
  LayerWeights, HiddenLayerWeights, OutputLayerWeights: TNeuralFloatArray;
  WeightElementList: TDOMElementList;
  WeightElement: TDOMElement;
  sWeight: String;
  Weight: TNeuralFloat;
  NeuronWeightCount: Integer;
begin
  try
    FS := GetCOCRFormatSettings;
    // Read the XML document into an object
    ReadXMLFile(Doc, FilePath);
    // Get the root (network) element
    NetworkElement := Doc.DocumentElement;
    if NetworkElement = nil then
      raise ENetworkError.Create('Error loading the network: Missing network ' +
        'node');
    // Read the number of inputs
    if NetworkElement.hasAttribute(txtNumInputs) then
    begin
      sNumInputs := NetworkElement[txtNumInputs];
      iNumInputs := StrToInt(sNumInputs);
    end
    else
      iNumInputs := 0;
    // Get layers
    LayerElementList := TDOMElementList.Create(NetworkElement, txtLayer);
    // Go through the layer element list and read the layers
    for L := 0 to LayerElementList.Count - 1 do
    begin
      // Initialise weight array
      LayerWeights := TNeuralFloatArray.Create;
      // Get current layer element
      LayerElement := LayerElementList[L] as TDOMElement;
      // Element type check
      if LayerElement.NodeType = ELEMENT_NODE then
      begin
        // If it has a bias attribute, read it
        if LayerElement.hasAttribute(txtBias) then
        begin
          sLayerBias := LayerElement[txtBias];
          LayerBias := StrToFloat(sLayerBias, FS);
        end
        else
          LayerBias := 1;
        // Get the neuron element list
        NeuronElementList := TDOMElementList.Create(LayerElement, txtNeuron);
        // Get neuron count
        iNumNeurons := NeuronElementList.Count;
        { Go through the neuron elements in the layer element and read the
          neurons }
        for N := 0 to iNumNeurons - 1 do
        begin
          // Get the neuron element
          NeuronElement := NeuronElementList[N] as TDOMElement;
          // Element type check
          if NeuronElement.NodeType = ELEMENT_NODE then
          begin
            // Get the weight element list
            WeightElementList := TDOMElementList.Create(NeuronElement,
              txtWeight);
            // Get the weight element count for current neuron element
            NeuronWeightCount := WeightElementList.Count;
            //WriteLn('Neuron weight count: ', NeuronWeightCount);
            // Go through the current neuron's weights and read them
            for W := 0 to NeuronWeightCount - 1 do
            begin
              // Get the weight element
              WeightElement := WeightElementList[W] as TDOMElement;
              // Element type chceck
              if WeightElement.NodeType = ELEMENT_NODE then
              begin
                { If it has the Value attribute, read it and append it to the
                  weight array }
                if WeightElement.hasAttribute(txtValue) then
                begin
                  sWeight := WeightElement[txtValue];
                  Weight := StrToFloat(sWeight, FS);
                  AppendNeuralFloat(LayerWeights, Weight);
                end;
              end;
            end;
          end;
        end;
        { Now a tricky final assignment. Because we have the hidden layer
          and the output layer hardcoded, we have to iterate from 0 to 1
          between them and assign their respective values separately. }
        if L = 0 then // Hidden layer
        begin
          iNumHidden := iNumNeurons;
          HiddenLayerBias := LayerBias;
          HiddenLayerWeights := LayerWeights;
        end
        else if L = 1 then // Output layer
        begin
          iNumOutputs := iNumNeurons;
          OutputLayerBias := LayerBias;
          OutputLayerWeights := LayerWeights;
        end;
      end;
    end;

    { Now that we have read all the needed information about the network
      we can call the constructor and give it everything it wants. }
    Create(iNumInputs, iNumHidden, iNumOutputs,
      HiddenLayerWeights, HiddenLayerBias,
      OutputLayerWeights, OutputLayerBias);

    // Read the learning rate
    if NetworkElement.hasAttribute(txtLearningRate) then
    begin
      sLearningRate := NetworkElement[txtLearningRate];
      flLearningRate := StrToFloat(sLearningRate, FS);
      LEARNING_RATE := flLearningRate;
    end; // The deafult value is already set
    {// Read the output threshold
    if NetworkElement.hasAttribute(txtOutputThreshold) then
    begin
      sOutputThreshold := NetworkElement[txtOutputThreshold];
      sngOutputThreshold := StrToFloat(sOutputThreshold);
      OUTPUT_THRESHOLD := sngOutputThreshold;
    end; // The deafult value is already set}
    // Read the iteration count
    if NetworkElement.hasAttribute(txtIterationCount) then
    begin
      sIterationCount := NetworkElement[txtIterationCount];
      qIterationCount := StrToQWord(sIterationCount);
      IterationCount := qIterationCount;
    end;
    // Read the epoch count
    if NetworkElement.hasAttribute(txtEpochCount) then
    begin
      sEpochCount := NetworkElement[txtEpochCount];
      qEpochCount := StrToQWord(sEpochCount);
      EpochCount := qEpochCount;
    end;
    // Read the absolute match achieved property
    if NetworkElement.hasAttribute(txtAbsoluteMatchAchieved) then
    begin
      sAbsoluteMatchAchieved := NetworkElement[txtAbsoluteMatchAchieved];
      bAbsoluteMatchAchieved := StrToBoolDef(sAbsoluteMatchAchieved, False);
      AbsoluteMatchAchieved := bAbsoluteMatchAchieved;
    end;
    // Read the text to teach property
    if NetworkElement.hasAttribute(txtCharsToTeach) then
    begin
      sCharsToTeach := NetworkElement[txtCharsToTeach];
      CharsToTeach := sCharsToTeach;
    end;
    // Read the “teach the characters to teach only” property
    if NetworkElement.hasAttribute(txtTeachCharsToTeachOnly) then
    begin
      sTeachCharsToTeachOnly := NetworkElement[txtTeachCharsToTeachOnly];
      bTeachCharsToTeachOnly := StrToBoolDef(sTeachCharsToTeachOnly, True);
      TeachCharsToTeachOnly := bTeachCharsToTeachOnly;
    end;
  { In every case, we need to free up the memory associated with the XML
    objects we created. }
  finally
    WeightElementList.Free;
    NeuronElementList.Free;
    LayerElementList.Free;
    NetworkElement.Free;
    Doc.Free;
  end;
end;

destructor TNeuralNetwork.Destroy;
begin
  hidden_layer.Free;
  output_layer.Free;
end;

{ def init_weights_from_inputs_to_hidden_layer_neurons(self,
    hidden_layer_weights): }
procedure TNeuralNetwork.init_weights_from_inputs_to_hidden_layer_neurons(
  hidden_layer_weights: array of TNeuralFloat);
var
  weight_num: Integer;
  i, h: Integer;
begin
  // weight_num = 0
  weight_num := 0;
  // for h in range(len(self.hidden_layer.neurons)):
  for h := 0 to high(Self.hidden_layer.neurons) do
  begin
    // for i in range(self.num_inputs):
    for i := 0 to Self.num_inputs - 1 do
    begin
      // if not hidden_layer_weights:
      if IsEmptyArray(hidden_layer_weights) then
        // self.hidden_layer.neurons[h].weights.append(random.random())
        AppendNeuralFloat(Self.hidden_layer.neurons[h].weights, Random)
      // else:
      else
        { self.hidden_layer.neurons[h].weights.append(
            hidden_layer_weights[weight_num]) }
        AppendNeuralFloat(Self.hidden_layer.neurons[h].weights,
          hidden_layer_weights[weight_num]);
      // weight_num += 1
      weight_num += 1;
    end;
  end;
end;

{ def init_weights_from_hidden_layer_neurons_to_output_layer_neurons(self,
    output_layer_weights): }
procedure TNeuralNetwork.
  init_weights_from_hidden_layer_neurons_to_output_layer_neurons
  (output_layer_weights: array of TNeuralFloat);
var
  weight_num: Integer;
  h, o: Integer;
begin
  // weight_num = 0
  weight_num := 0;
  // for o in range(len(self.output_layer.neurons)):
  for o := 0 to high(Self.output_layer.neurons) do
  begin
    // for h in range(len(self.hidden_layer.neurons)):
    for h := 0 to High(self.hidden_layer.neurons) do
    begin
      //if not output_layer_weights:
      if IsEmptyArray(output_layer_weights) then
        // self.output_layer.neurons[o].weights.append(random.random())
        AppendNeuralFloat(Self.output_layer.neurons[o].weights, Random)
      // else:
      else
        { self.output_layer.neurons[o].weights.append(
            output_layer_weights[weight_num]) }
        AppendNeuralFloat(Self.output_layer.neurons[o].weights,
          output_layer_weights[weight_num]);
      // weight_num += 1
      weight_num += 1;
    end;
  end;
end;

// def inspect(self):
procedure TNeuralNetwork.inspect();
begin
  // print('------')
  WriteLn('------');
  // print('* Inputs: {}'.format(self.num_inputs))
  WriteLn(Format('* Inputs: %d', [Self.num_inputs]));
  // print('------')
  WriteLn('------');
  // print('Hidden Layer')
  WriteLn('Hidden Layer');
  // self.hidden_layer.inspect()
  Self.hidden_layer.inspect();
  // print('------')
  WriteLn('------');
  // print('* Output Layer')
  WriteLn('* Output Layer');
  // self.output_layer.inspect()
  Self.output_layer.inspect();
  // print('------')
  WriteLn('------');
end;

// def feed_forward(self, inputs):
function TNeuralNetwork.feed_forward(inputs: TNeuralFloatArray):
  TNeuralFloatArray;
var
  hidden_layer_outputs: TNeuralFloatArray;
begin
  // hidden_layer_outputs = self.hidden_layer.feed_forward(inputs)
  hidden_layer_outputs := Self.hidden_layer.feed_forward(inputs);
  // return self.output_layer.feed_forward(hidden_layer_outputs)
  Result := Self.output_layer.feed_forward(hidden_layer_outputs);
end;

// Uses online learning, ie updating the weights after each training case
// def train(self, training_inputs, training_outputs):
procedure TNeuralNetwork.train(training_inputs,
  training_outputs: TNeuralFloatArray);
var
  pd_errors_wrt_output_neuron_total_net_input: TNeuralFloatArray;
  pd_errors_wrt_hidden_neuron_total_net_input: TNeuralFloatArray;
  h, o: Integer;
  d_error_wrt_hidden_neuron_output: TNeuralFloat;
  w_ho, w_ih: Integer;
  pd_error_wrt_weight: TNeuralFloat;
begin
  // self.feed_forward(training_inputs)
  Self.feed_forward(training_inputs);

  // 1. Output neuron deltas
  { pd_errors_wrt_output_neuron_total_net_input =
      [0] * len(self.output_layer.neurons) }
  pd_errors_wrt_output_neuron_total_net_input := TNeuralFloatArray.Create;
  SetLength(pd_errors_wrt_output_neuron_total_net_input,
    length(self.output_layer.neurons));
  // for o in range(len(self.output_layer.neurons)):
  for o := 0 to High(Self.output_layer.neurons) do

      // ∂E/∂zⱼ
      { pd_errors_wrt_output_neuron_total_net_input[o] =
          self.output_layer.neurons[o].calculate_pd_error_wrt_total_net_input(
            training_outputs[o]) }
      pd_errors_wrt_output_neuron_total_net_input[o] :=
        Self.output_layer.neurons[o].calculate_pd_error_wrt_total_net_input(
          training_outputs[o]);

  // 2. Hidden neuron deltas
  { pd_errors_wrt_hidden_neuron_total_net_input =
      [0] * len(self.hidden_layer.neurons) }
  pd_errors_wrt_hidden_neuron_total_net_input := TNeuralFloatArray.Create;
  SetLength(pd_errors_wrt_hidden_neuron_total_net_input,
    Length(Self.hidden_layer.neurons));
  // for h in range(len(self.hidden_layer.neurons)):
  for h := 0 to High(Self.hidden_layer.neurons) do
  begin

      { We need to calculate the derivative of the error with respect
        to the output of each hidden layer neuron
        dE/dyⱼ = Σ ∂E/∂zⱼ * ∂z/∂yⱼ = Σ ∂E/∂zⱼ * wᵢⱼ }
      // d_error_wrt_hidden_neuron_output = 0
      d_error_wrt_hidden_neuron_output := 0;

      // for o in range(len(self.output_layer.neurons)):
      for o := 0 to High(Self.output_layer.neurons) do
          { d_error_wrt_hidden_neuron_output +=
              pd_errors_wrt_output_neuron_total_net_input[o] *
              self.output_layer.neurons[o].weights[h] }
          d_error_wrt_hidden_neuron_output +=
            pd_errors_wrt_output_neuron_total_net_input[o] *
            Self.output_layer.neurons[o].weights[h];

      // ∂E/∂zⱼ = dE/dyⱼ * ∂zⱼ/∂
      { pd_errors_wrt_hidden_neuron_total_net_input[h] =
          d_error_wrt_hidden_neuron_output *
          self.hidden_layer.neurons[h].calculate_d_total_net_input_wrt_input() }
      pd_errors_wrt_hidden_neuron_total_net_input[h] :=
        d_error_wrt_hidden_neuron_output *
        Self.hidden_layer.neurons[h].calculate_d_total_net_input_wrt_input();
  end;

  // 3. Update output neuron weights
  // for o in range(len(self.output_layer.neurons)):
  for o := 0 to High(Self.output_layer.neurons) do
  begin
    // for w_ho in range(len(self.output_layer.neurons[o].weights)):
    for w_ho := 0 to High(Self.output_layer.neurons[o].weights) do
    begin

      // ∂Eⱼ/∂wᵢⱼ = ∂E/∂zⱼ * ∂zⱼ/∂wᵢⱼ
      { pd_error_wrt_weight = pd_errors_wrt_output_neuron_total_net_input[o] *
          self.output_layer.neurons[o].calculate_pd_total_net_input_wrt_weight(
            w_ho) }
      pd_error_wrt_weight :=  pd_errors_wrt_output_neuron_total_net_input[o] *
        self.output_layer.neurons[o].calculate_pd_total_net_input_wrt_weight(
          w_ho);

      // Δw = α * ∂Eⱼ/∂wᵢ
      { self.output_layer.neurons[o].weights[w_ho] -=
          self.LEARNING_RATE * pd_error_wrt_weight }
      Self.output_layer.neurons[o].weights[w_ho] -=
        self.LEARNING_RATE * pd_error_wrt_weight;
    end;
  end;

  // 4. Update hidden neuron weights
  // for h in range(len(self.hidden_layer.neurons)):
  for h := 0 to High(Self.hidden_layer.neurons) do
  begin
    // for w_ih in range(len(self.hidden_layer.neurons[h].weights)):
    for w_ih := 0 to High(Self.hidden_layer.neurons[h].weights) do
    begin
      // ∂Eⱼ/∂wᵢ = ∂E/∂zⱼ * ∂zⱼ/∂wᵢ
      { pd_error_wrt_weight = pd_errors_wrt_hidden_neuron_total_net_input[h] *
          self.hidden_layer.neurons[h].calculate_pd_total_net_input_wrt_weight(
            w_ih) }
      pd_error_wrt_weight := pd_errors_wrt_hidden_neuron_total_net_input[h] *
        Self.hidden_layer.neurons[h].calculate_pd_total_net_input_wrt_weight(
          w_ih);

      // Δw = α * ∂Eⱼ/∂wᵢ
      { self.hidden_layer.neurons[h].weights[w_ih] -=
          self.LEARNING_RATE * pd_error_wrt_weight }
      Self.hidden_layer.neurons[h].weights[w_ih] -=
        Self.LEARNING_RATE * pd_error_wrt_weight;
    end;
  end;

  // Network weights got modified, let's set the Modified flag
  Modified := True;

  // Increase the iteration count; epoch count has to be increased from outside
  IterationCount := IterationCount + 1;
end;

procedure TNeuralNetwork.train(training_set: TTrainingSet);
begin
  train(training_set[tsptInput], training_set[tsptOutput]);
end;

// def calculate_total_error(self, training_sets):
function TNeuralNetwork.calculate_total_error(training_sets: TTrainingSets
  ): TNeuralFloat;
var
  total_error: TNeuralFloat;
  t, o: Integer;
  training_inputs, training_outputs: TNeuralFloatArray;
begin
  //total_error = 0
  total_error := 0;
  // for t in range(len(training_sets)):
  for t := 0 to High(training_sets) do
  begin
    // training_inputs, training_outputs = training_sets[t]
    training_inputs := training_sets[t][tsptInput];
    training_outputs := training_sets[t][tsptOutput];
    // self.feed_forward(training_inputs)
    Self.feed_forward(training_inputs);
    // for o in range(len(training_outputs)):
    for o := 0 to High(training_outputs) do
      { total_error +=
          self.output_layer.neurons[o].calculate_error(training_outputs[o]) }
      total_error +=
        Self.output_layer.neurons[o].calculate_error(training_outputs[o]);
  end;
  // return total_error
  Result := total_error;
end;

procedure TNeuralNetwork.SaveYourself(Path: String);
var
  FS: TFormatSettings;
  Doc: TXMLDocument;
  NetworkElement: TDOMElement;
  sLearningRate: String;
  //sOutputThreshold: String;
  sNumInputs: String;
  sIterationCount: String;
  sEpochCount: String;
  sAbsoluteMatchAchieved: String;
  sTeachCharsToTeachOnly: String;
begin
  try
    FS := GetCOCRFormatSettings;
    Doc := TXMLDocument.Create;
    NetworkElement := Doc.CreateElement('Network');
    sLearningRate := FloatToStr(LEARNING_RATE, FS);
    NetworkElement[txtLearningRate] := sLearningRate;
    //sOutputThreshold := FloatToStr(OUTPUT_THRESHOLD);
    //NetworkElement[txtOutputThreshold] := sOutputThreshold;
    sNumInputs := IntToStr(num_inputs);
    NetworkElement[txtNumInputs] := sNumInputs;
    sIterationCount := IntToStr(IterationCount);
    NetworkElement[txtIterationCount] := sIterationCount;
    sEpochCount := IntToStr(EpochCount);
    NetworkElement[txtEpochCount] := sEpochCount;
    sAbsoluteMatchAchieved := BoolToStr(FAbsoluteMatchAchieved, True);
    NetworkElement[txtAbsoluteMatchAchieved] := sAbsoluteMatchAchieved;
    NetworkElement[txtCharsToTeach] := FCharsToTeach;
    sTeachCharsToTeachOnly := BoolToStr(FTeachCharsToTeachOnly, True);
    NetworkElement[txtTeachCharsToTeachOnly] := sTeachCharsToTeachOnly;
    hidden_layer.SaveYourself(Doc, NetworkElement);
    output_layer.SaveYourself(Doc, NetworkElement);
    Doc.AppendChild(NetworkElement);
    WriteXMLFile(Doc, Path);
    Modified := False;
  finally
    Doc.Free;
  end;
end;

procedure TNeuralNetwork.EpochDone;
begin
  EpochCount := EpochCount + 1;
end;

{ TNeuronLayer }

function TNeuronLayer.GetCount: Integer;
begin
  Result := Length(neurons);
end;

function TNeuronLayer.GetItem(Index: Integer): TNeuron;
begin
  Result := neurons[Index];
end;

//def __init__(self, num_neurons, bias):
constructor TNeuronLayer.Create(num_neurons: Integer; initial_bias: TNeuralFloat);
var i: Integer;
begin
  // Every neuron in a layer shares the same bias
  //self.bias = bias if bias else random.random()
  if initial_bias > 0 then
    Self.bias := initial_bias
  else
    Self.bias := Random;

  {self.neurons = []
  for i in range(num_neurons):
    self.neurons.append(Neuron(self.bias))}
  SetLength(Self.neurons, num_neurons);
  for i := 0 to num_neurons - 1 do
    Self.neurons[i] := TNeuron.Create(Self.bias);

end;

destructor TNeuronLayer.Destroy;
var
  I: Integer;
  neuron: TNeuron;
begin
  for I := 0 to High(neurons) do
  begin
    neuron := neurons[I];
    neuron.Free;
  end;
end;

// def inspect(self):
procedure TNeuronLayer.inspect;
var
  n, w: Integer;
begin
  // print('Neurons:', len(self.neurons))
  WriteLn('Neurons:', Length(Self.neurons));
  // for n in range(len(self.neurons)):
  for n := 0 to High(Self.neurons) do
  begin
    // print(' Neuron', n)
    WriteLn(' Neuron', n);
    //for w in range(len(self.neurons[n].weights)):
    for w := 0 to High(Self.neurons[n].weights) do
      // print('  Weight:', self.neurons[n].weights[w])
      WriteLn('  Weight:', Self.neurons[n].weights[w]);
  end;
  // print(' Bias:', self.bias)
  WriteLn(' Bias:', Self.bias);
end;

// def feed_forward(self, inputs):
function TNeuronLayer.feed_forward(inputs: TNeuralFloatArray): TNeuralFloatArray;
var
  outputs: TNeuralFloatArray;
  n: Integer;
  neuron: TNeuron;
begin
  // outputs = []
  outputs := TNeuralFloatArray.Create;
  // for neuron in self.neurons:
  for n := 0 to High(Self.neurons) do
  begin
    neuron := Self.neurons[n];
    // outputs.append(neuron.calculate_output(inputs))
    AppendNeuralFloat(outputs, neuron.calculate_output(inputs));
  end;
  // return outputs
  Result := outputs;
end;

// def get_outputs(self):
function TNeuronLayer.get_outputs: TNeuralFloatArray;
var
  outputs: TNeuralFloatArray;
  n: integer;
  neuron: TNeuron;
begin
  // outputs = []
  outputs := TNeuralFloatArray.Create;
  // for neuron in self.neurons:
  for n := 0 to High(Self.neurons) do
  begin
    neuron := Self.neurons[n];
    // outputs.append(neuron.output)
    AppendNeuralFloat(outputs, neuron.output);
  end;
  // return outputs
  Result := outputs;
end;

procedure TNeuronLayer.SaveYourself(Doc: TXMLDocument;
  NetworkElement: TDOMElement);
var
  LayerElement: TDOMElement;
  N: Integer;
  objN: TNeuron;
  sBias: String;
  FS: TFormatSettings;
begin
  LayerElement := Doc.CreateElement('Layer');
  FS := DefaultFormatSettings;
  FS.DecimalSeparator := '.';
  sBias := FloatToStr(bias, FS);
  LayerElement.SetAttribute(txtBias, sBias);
  for N := 0 to Count - 1 do
  begin
    objN := Items[N];
    objN.SaveYourself(Doc, LayerElement);
  end;
  NetworkElement.AppendChild(LayerElement);
end;

{ TNeuron }

//def __init__(self, bias):
constructor TNeuron.Create(initial_bias: TNeuralFloat);
begin
  //self.bias = bias
  Self.bias := initial_bias;
  //self.weights = []
  SetLength(Self.weights, 0);
end;

// def calculate_output(self, inputs):
function TNeuron.calculate_output(inputs_: TNeuralFloatArray): TNeuralFloat;
begin
    // self.inputs = inputs
    Self.inputs := inputs_;
    // self.output = self.squash(self.calculate_total_net_input())
    Self.output := Self.squash(self.calculate_total_net_input());
    // return self.output
    Result := Self.output;
end;

// def calculate_total_net_input(self):
function TNeuron.calculate_total_net_input: TNeuralFloat;
var
  total: TNeuralFloat;
  i: Integer;
begin
  // total = 0
  total := 0;
  // for i in range(len(self.inputs)):
  for i := 0 to high(Self.inputs) do
  begin
    // total += self.inputs[i] * self.weights[i]
    total += Self.inputs[i] * Self.weights[i];
  end;
  // return total + self.bias
  Result := total + Self.bias;
end;

{ Apply the logistic function to squash the output of the neuron
  The result is sometimes referred to as 'net' [2] or 'net' [1] }
// def squash(self, total_net_input):
function TNeuron.squash(total_net_input: TNeuralFloat): TNeuralFloat;
begin
  // return 1 / (1 + math.exp(-total_net_input))
  Result := 1 / (1 + exp(-total_net_input));
end;

{ Determine how much the neuron's total input has to change to move closer to
  the expected output

  Now that we have the partial derivative of the error with respect to the
  output (∂E/∂yⱼ) and the derivative of the output with respect to the total
  net input (dyⱼ/dzⱼ) we can calculate the partial derivative of the error with
  respect to the total net input.
  This value is also known as the delta (δ) [1]
  δ = ∂E/∂zⱼ = ∂E/∂yⱼ * dyⱼ/dzⱼ }
// def calculate_pd_error_wrt_total_net_input(self, target_output):
function TNeuron.calculate_pd_error_wrt_total_net_input(
  target_output: TNeuralFloat): TNeuralFloat;
begin
  { return self.calculate_pd_error_wrt_output(target_output) *
      self.calculate_pd_total_net_input_wrt_input() }
  Result := Self.calculate_pd_error_wrt_output(target_output) *
    Self.calculate_d_total_net_input_wrt_input();
end;

// The error for each neuron is calculated by the Mean Square Error method:
// def calculate_error(self, target_output):
function TNeuron.calculate_error(target_output: TNeuralFloat): TNeuralFloat;
begin
  // return 0.5 * (target_output - self.output) ** 2
  Result := 0.5 * sqr(target_output - Self.output);
end;

{ The partial derivate of the error with respect to actual output then is
  calculated by:
  = 2 * 0.5 * (target output - actual output) ^ (2 - 1) * -1
  = -(target output - actual output)

  The Wikipedia article on backpropagation [1] simplifies to the following
  but most other learning material does not [2]
  = actual output - target output

  Alternatively, you can use (target - output) but then need to add it
  during backpropagation [3]

  Note that the actual output of the output neuron is often written as yⱼ and
  target output as tⱼ so:
  = ∂E/∂yⱼ = -(tⱼ - yⱼ) }
// def calculate_pd_error_wrt_output(self, target_output):
function TNeuron.calculate_pd_error_wrt_output(target_output: TNeuralFloat
  ): TNeuralFloat;
begin
  // return -(target_output - self.output)
  Result := -(target_output - Self.output);
end;

{ The total net input into the neuron is squashed using logistic function
  to calculate the neuron's output:
  yⱼ = φ = 1 / (1 + e^(-zⱼ))
  Note that where ⱼ represents the output of the neurons in whatever layer
  we're looking at and ᵢ represents the layer below it

  The derivative (not partial derivative since there is only one variable)
  of the output then is:
  dyⱼ/dzⱼ = yⱼ * (1 - yⱼ)}
// def calculate_d_total_net_input_wrt_input(self):
function TNeuron.calculate_d_total_net_input_wrt_input: TNeuralFloat;
begin
  // return self.output * (1 - self.output)
  Result := Self.output * (1 - Self.output);
end;

{ The total net input is the weighted sum of all the inputs to the neuron
  and their respective weights:
  = zⱼ = netⱼ = x₁w₁ + x₂w₂ ...

  The partial derivative of the total net input with respective to a given
  weight (with everything else held constant) then is:
  = ∂zⱼ/∂wᵢ = some constant + 1 * xᵢw₁^(1-0) + some constant ... = xᵢ }
// def calculate_pd_total_net_input_wrt_weight(self, index):
function TNeuron.calculate_pd_total_net_input_wrt_weight(index: Integer
  ): TNeuralFloat;
begin
  // return self.inputs[index]
  Result := Self.inputs[index];
end;

function TNeuron.GetMinWeight: TNeuralFloat;
var
  W: Integer;
begin
  Result := weights[0];
  for W := 1 to High(weights) do
  begin
    if weights[W] < Result then
    begin
      Result := weights[W];
    end;
  end;
end;

function TNeuron.GetMaxWeight: TNeuralFloat;
var
  W: Integer;
begin
  Result := weights[0];
  for W := 1 to High(weights) do
  begin
    if weights[W] > Result then
    begin
      Result := weights[W];
    end;
  end;
end;

procedure TNeuron.SaveYourself(Doc: TXMLDocument; LayerElement: TDOMElement);
var
  NeuronElement: TDOMElement;
  W, HighestWeightIndex: Integer;
  Weight: TNeuralFloat;
  sWeight: String;
  WeightElement: TDOMElement;
  FS: TFormatSettings;
begin
  NeuronElement := Doc.CreateElement(txtNeuron);
  FS := DefaultFormatSettings;
  FS.DecimalSeparator := '.';
  HighestWeightIndex := High(weights);
  //WriteLn('Highest weight index: ', HighestWeightIndex);
  for W := 0 to HighestWeightIndex do
  begin
    Weight := weights[W];
    sWeight := FloatToStr(Weight, FS);
    WeightElement := Doc.CreateElement(txtWeight);
    WeightElement[txtValue] := sWeight;
    NeuronElement.AppendChild(WeightElement);
  end;
  LayerElement.AppendChild(NeuronElement);
end;

function IsEmptyArray(TheArray: array of TNeuralFloat): Boolean;
begin
  if Length(TheArray) = 0 then
    Result := True
  else
    Result := False;
end;

procedure AppendNeuralFloat(var TheArray: TNeuralFloatArray; Item: TNeuralFloat);
begin
  SetLength(TheArray, Length(TheArray) + 1);
  TheArray[High(TheArray)] := Item;
end;

function CreateTrainingSet(Inputs: TNeuralFloatArray; Outputs: TNeuralFloatArray
  ): TTrainingSet;
begin
  Result[tsptInput] := Inputs;
  Result[tsptOutput] := Outputs;
end;

function FormatNeuralFloatArray(FormatStr: String; NeuralFloatArray: TNeuralFloatArray
  ): String;
var
  I: Integer;
begin
  Result := '[';
  for I := 0 to High(NeuralFloatArray) do
  begin
    if I > 0 then Result += ', ';
    Result += FormatFloat(FormatStr, NeuralFloatArray[I]);
  end;
  Result += ']';
end;

function GetOptimalNumberOfHiddenNeurons(NumberOfInputs,
  NumberOfOutputs, NumberOfSamples, ScalingFactor: Integer): Integer;
begin
  // See "Number of Hidden Neurons.png"
  {           Ns
    Nh = _______________

          α * (Ni + No)

    Nh = optimal number of hidden neurons
    Ns = number of samples in the trainig set (e. g. number of characters)
    Ni = number of input neurons
    No = number of output neurons
    α  = scaling factor, α ∈ <2,10>
  }
  Result :=
    NumberOfSamples div (ScalingFactor * (NumberOfInputs + NumberOfOutputs));
end;



end.

