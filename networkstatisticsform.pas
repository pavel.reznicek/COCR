unit NetworkStatisticsForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, sqlite3conn, sqldb, Forms, Controls, Graphics, Dialogs,
  Grids, DBGrids, sqlscript, db;

type

  { TFrmNetworkStatistics }

  TFrmNetworkStatistics = class(TForm)
    DS: TDataSource;
    Grid: TDBGrid;
    Connection: TSQLite3Connection;
    Query: TSQLQuery;
    Script: TSQLScript;
    Transaction: TSQLTransaction;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure ScriptDirective(Sender: TObject; Directive, Argument: AnsiString;
      var StopExecution: Boolean);
  private
    function AddNetworkLine(Path: String; Index: Integer): Boolean;
  public

  end;

var
  FrmNetworkStatistics: TFrmNetworkStatistics;

implementation

uses FileUtil, BetaNetwork;

{$R *.lfm}

{ TFrmNetworkStatistics }

procedure TFrmNetworkStatistics.FormShow(Sender: TObject);
var
  FileList: TStringList;
  N: Integer;
begin
  Connection.DatabaseName:=ExtractFilePath(Application.ExeName) +
    'NetworkStatistics.sqlite';
  Connection.Connected:=True;
  Script.ExecuteScript;
  Query.Active:=True;
  try
    FileList := FindAllFiles('', '*.net');
    for N:=0 to Pred(FileList.Count) do
    begin
        AddNetworkLine(FileList[N], N);
    end;
    Query.Close;
    Query.Open;
  finally
    FreeAndNil(FileList);
  end;
end;

procedure TFrmNetworkStatistics.FormCreate(Sender: TObject);
begin

end;

procedure TFrmNetworkStatistics.ScriptDirective(Sender: TObject; Directive,
  Argument: AnsiString; var StopExecution: Boolean);
begin

end;

function TFrmNetworkStatistics.AddNetworkLine(Path: String; Index: Integer): Boolean;
  var
    FixedRowsCount: Integer;
    DataRowCount: Integer;
    NeededDataRowCount: Integer;
    NeededRowCount: Integer;
    RowIndex: Integer;
    Row: TStrings;
    Network: TNeuralNetwork;
begin
  Result:=False;
  Query.Append;
  // Fill the file path in
  Query.FieldByName('name').AsString := Path;
  try
    // Load the network object from the  given file path
    Network := TNeuralNetwork.Create(Path);
    // Fill the CharsToTeach property in
    Query.FieldByName('chars_to_teach').AsString := Network.CharsToTeach;
    // Fill the hidden layer neuron count in
    Query.FieldByName('hidden_layer').AsInteger := Network.HiddenLayer.Count;
    // Fill the learning rate (rounded to thee decimal places) in
    Query.FieldByName('learning_rate').AsFloat := Network.LearningRate;
    // Fill the epoch count in
    Query.FieldByName('epoch_count').AsInteger:=Network.EpochCount;
    // Fill the boolean absolute match indicator in
    Query.FieldByName('absolute_match').AsBoolean:=
      Network.AbsoluteMatchAchieved;
    // Save the row
    Query.Post;
    Query.ApplyUpdates;
    // Success!
    Result:=True;
  finally
    // Make sure tha used memory gets freed
    FreeAndNil(Network);
  end;
end;

end.

