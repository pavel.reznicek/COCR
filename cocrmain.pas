unit COCRMain;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, LResources, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, ExtDlgs, Spin, ComCtrls, LCLType, Arrow, ValEdit, types,
  XMLConf, XMLRead, LazFileUtils, Grids, Menus, BetaNetwork, COCRUtils,
  uplaysound;

type
  { TfCOCR }
  TfCOCR = class(TForm)
    arLeft: TArrow;
    arRight: TArrow;
    arUp: TArrow;
    arDown: TArrow;
    bevCursor: TBevel;
    btTestOneChar: TButton;
    cbTeachCharsToTeachOnly: TCheckBox;
    edCorrection: TEdit;
    edCharsToTeach: TEdit;
    gbList: TGroupBox;
    gbLog: TGroupBox;
    gbExtendedCharacterLearning: TGroupBox;
    gbSingleCharacterLearning: TGroupBox;
    gbTeachingTest: TGroupBox;
    imgBits: TImage;
    imgSourceImage: TImage;
    imgSynapses: TImage;
    imgZoom: TImage;
    lblErrorTitle: TLabel;
    lblEpochErrorTitle: TLabel;
    lblError: TLabel;
    lblEpochError: TLabel;
    lbIsThisCharacterSelected: TLabel;
    lblLeft: TLabel;
    lblCharsToTeach: TLabel;
    lblWidth: TLabel;
    lblHeight: TLabel;
    lblGuess: TLabel;
    lblGuessTitle: TLabel;
    lblCorrection: TLabel;
    lblTop: TLabel;
    mmCOCR: TMemo;
    mmLog: TMemo;
    odOpen: TOpenDialog;
    panArrows: TPanel;
    panTextToTeachLabel: TPanel;
    panLeft: TPanel;
    panLog: TPanel;
    panNetworkProperties: TGroupBox;
    panProgress: TPanel;
    panSourceControl: TPanel;
    panCharacters: TPanel;
    pbProgress: TProgressBar;
    pcPages: TPageControl;
    PlaySound: Tplaysound;
    PropEditor: TValueListEditor;
    sdSave: TSaveDialog;
    sbSourceImage: TScrollBox;
    seLeft: TSpinEdit;
    seTop: TSpinEdit;
    seWidth: TSpinEdit;
    seHeight: TSpinEdit;
    lsCharacters: TListBox;
    splRight: TSplitter;
    splBottom: TSplitter;
    splLeft: TSplitter;
    tbCorrect: TToggleBox;
    tbTeachTheNetwork: TToggleBox;
    btYes: TButton;
    btLoadSourceImage: TButton;
    btNo: TButton;
    btRefreshGuess: TButton;
    tbLog: TToolBar;
    tbSaveLog: TToolButton;
    tbNewLog: TToolButton;
    tbSaveAs: TToolButton;
    tsSourceImage: TTabSheet;
    tsAbout: TTabSheet;
    opdOpenPic: TOpenPictureDialog;
    XMLC: TXMLConfig;
    IL: TImageList;
    TB: TToolBar;
    tbNew: TToolButton;
    tbOpen: TToolButton;
    tbSave: TToolButton;
    procedure bevCursorChangeBounds(Sender: TObject);
    procedure btnCreateNetworkClick(Sender: TObject);
    procedure btTestOneCharClick(Sender: TObject);
    procedure cbTeachCharsToTeachOnlyChange(Sender: TObject);
    procedure edCorrectionChange(Sender: TObject);
    procedure edCorrectionKeyPress(Sender: TObject; var Key: char);
    procedure edCharsToTeachChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
    procedure FormCreate(Sender: TObject);
    procedure imgBitsClick(Sender: TObject);
    procedure imgBitsPaint(Sender: TObject);
    procedure imgSourceImageMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure imgSourceImageMouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure imgSourceImageMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: integer);
    procedure imgSourceImagePaint(Sender: TObject);
    procedure imgSynapsesClick(Sender: TObject);
    procedure imgSynapsesPaint(Sender: TObject);
    procedure lblCharsToTeachClick(Sender: TObject);
    procedure PropEditorHeaderSized(Sender: TObject; IsColumn: Boolean;
      Index: Integer);
    procedure PropEditorValidateEntry(sender: TObject; aCol, aRow: Integer;
      const OldValue: string; var NewValue: String);
    procedure seLeftChange(Sender: TObject);
    procedure seTopChange(Sender: TObject);
    procedure seWidthChange(Sender: TObject);
    procedure seHeightChange(Sender: TObject);
    procedure lsCharactersClick(Sender: TObject);
    procedure lsCharactersDblClick(Sender: TObject);
    procedure lsCharactersDrawItem(Control: TWinControl; Index: integer;
      ARect: TRect; State: TOwnerDrawState);
    procedure lsCharactersKeyPress(Sender: TObject; var Key: char);
    procedure lsCharactersUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char);
    procedure arLeftClick(Sender: TObject);
    procedure arDownClick(Sender: TObject);
    procedure arRightClick(Sender: TObject);
    procedure arUpClick(Sender: TObject);
    procedure tbCorrectChange(Sender: TObject);
    procedure tbNewClick(Sender: TObject);
    procedure tbNewLogClick(Sender: TObject);
    procedure tbOpenClick(Sender: TObject);
    procedure tbSaveAsClick(Sender: TObject);
    procedure tbSaveClick(Sender: TObject);
    procedure tbSaveLogClick(Sender: TObject);
    procedure tbTeachTheNetworkChange(Sender: TObject);
    procedure btYesClick(Sender: TObject);
    procedure btLoadSourceImageClick(Sender: TObject);
    procedure btnLoadNetworkClick(Sender: TObject);
    procedure btNoClick(Sender: TObject);
    procedure btRefreshGuessClick(Sender: TObject);
    procedure tbLogClick(Sender: TObject);
  private
    FLastNetworkFilePath: String;
    { private declarations }
    LeftMouseButtonDepressed: Boolean;
    DrawCharRect: Boolean;
    MoveFlag: Boolean;
    CorrectFlag: Boolean;
    NetworkPrepared: Boolean;
    FControlsEnabled: Boolean;
    HiddenNeuronToDrawIndex: Integer;
    FNetworkFilePath: String;
    FConfigFilePath: String;
    function GetTeachCharsToTeachOnly: Boolean;
    procedure SetLastNetworkFilePath(AValue: String);
    procedure SetNetworkFilePath(AValue: String);
    procedure SetTeachCharsToTeachOnly(AValue: Boolean);
    procedure NetworkModifiedChange(Sender: TObject);
  public
    { public declarations }
    Network: {TNetwork} TNeuralNetwork;
    InputPattern: {TDoubleArray} TNeuralFLoatArray;
    TargetOutputPattern: {TDoubleArray} TNeuralFLoatArray;
    function GetCursorRect: TRect;
    procedure SetCursorRect(TheCursor: TRect);
    procedure FillCharacterList;
    procedure Move;
    procedure RefreshCharts;
    function GetGlyphRect: TRect;
    procedure DrawZoom;
    procedure RefreshCorrection;
    function GetInputPattern: TNeuralFLoatArray;
    function GetNetworkGuess: UTF8String;
    //function GetNetworksOpinionAboutCharacter(Character: UnicodeChar): Extended;
    function GetCorrectionCharacter: UnicodeChar;
    //function GetNetworksOpinionAboutCorrectionCharacter: Extended;
    function GetNetworkError: String;
    procedure CreateNetwork;
    procedure LoadNetwork;
    procedure LoadNetworkFromFile(FilePath: String);
    procedure TrainNetwork;
    procedure SaveNetwork(SaveAs: Boolean);
    procedure LoadSourceImage(FileName: String);
    procedure SetControlsEnabled(Enable: Boolean);
    function GetCharsToTeach: UnicodeString;
    procedure SetCharsToTeach(TheChars: UnicodeString);
    procedure CreateSettings;
    procedure LoadSettings;
    procedure SaveSettings;
    procedure ShowNetworkProperties;
    procedure WriteToLog(Message: String);
    procedure ClearLog;
    procedure SaveLog;
    procedure RefreshCaption;
    function TestOneChar(Character: UnicodeChar): UnicodeChar;
    function CorrectOneChar(Character: UnicodeChar): UnicodeChar;
    function TeachOneChar(Character: UnicodeChar): TTeachingResult;
    function QueryNumberOfHiddenNeurons: Integer;
    procedure Correct;
    property CursorRect: TRect read GetCursorRect write SetCursorRect;
  published
    property ControlsEnabled: Boolean
      read FControlsEnabled write SetControlsEnabled;
    property CharsToTeach: UnicodeString read GetCharsToTeach write SetCharsToTeach;
    property TeachCharsToTeachOnly: Boolean read GetTeachCharsToTeachOnly
      write SetTeachCharsToTeachOnly;
    property NetworkFilePath: String read FNetworkFilePath
      write SetNetworkFilePath;
    property LastNetworkFilePath: String read FLastNetworkFilePath
      write SetLastNetworkFilePath;
  end;

var
  fCOCR: TfCOCR;

procedure DoBeep;

const
  DefaultSourceImageFileName: String = 'source.png';
  DefaultTextToTeach: WideString = 'abcdef';

  txtTextToTeach: WideString = 'TextToTeach';
  txtTeachCharsToTeachOnly: WideString = 'TeachCharsToTeachOnly';
  txtLastNetworkFilePath: WideString = 'LastNetworkFilePath';

implementation

procedure DoBeep;
begin
  fCOCR.PlaySound.SoundFile := ExtractFilePath(Application.ExeName) +
    'pyp.wav';
  fCOCR.PlaySound.Execute;
end;


{ TfCOCR }

procedure TfCOCR.btLoadSourceImageClick(Sender: TObject);
begin
  if opdOpenPic.Execute then
    LoadSourceImage(opdOpenPic.FileName);
end;

procedure TfCOCR.btnLoadNetworkClick(Sender: TObject);
begin
  LoadNetwork;
end;
procedure TfCOCR.btNoClick(Sender: TObject);
{var
  WCh: UnicodeChar;
  Kod: Word absolute WCh;
  N: TNeuron;
  //I: Integer;
}
begin
  {
  WCh := GetCorrectionCharacter;
  N := Network.output_layer.neurons[Kod];
  //while N.Hodnota > -1 do
  //for I := 0 to 9 do
  begin
    N.Evaluate(-1);
    RefreshCharts;
    Application.ProcessMessages;
  end;
  Move;
  //Beep;
  }
end;

procedure TfCOCR.btRefreshGuessClick(Sender: TObject);
begin
  lblGuess.Caption := GetNetworkGuess;
end;

procedure TfCOCR.tbLogClick(Sender: TObject);
begin

end;

function TfCOCR.GetTeachCharsToTeachOnly: Boolean;
begin
  Result := cbTeachCharsToTeachOnly.Checked;
end;

procedure TfCOCR.SetLastNetworkFilePath(AValue: String);
begin
  if FLastNetworkFilePath=AValue then Exit;
  FLastNetworkFilePath:=AValue;
end;

procedure TfCOCR.SetNetworkFilePath(AValue: String);
begin
  //if FNetworkFilePath = AValue then Exit;
  FNetworkFilePath := AValue;
  LastNetworkFilePath := AValue;
  RefreshCaption;
end;

procedure TfCOCR.SetTeachCharsToTeachOnly(AValue: Boolean);
begin
  cbTeachCharsToTeachOnly.Checked:=AValue;
end;

procedure TfCOCR.NetworkModifiedChange(Sender: TObject);
begin
  if Network.Modified then
    RefreshCaption;
end;

procedure TfCOCR.btnCreateNetworkClick(Sender: TObject);
begin
  CreateNetwork;
end;

procedure TfCOCR.btTestOneCharClick(Sender: TObject);
var
  Extent: TSize;
  sGuess: UTF8String;
  Character: UnicodeChar;
  ChosenCharNumber: Integer;
  locTextToTeach: UnicodeString;
  sInputPattern: String;
begin
  // Choose a character
  locTextToTeach := CharsToTeach;
  ChosenCharNumber := Random(Length(locTextToTeach)) + 1;
  Character := locTextToTeach[ChosenCharNumber];
  // TestOneChar function body
  imgSourceImage.Picture.Bitmap.Canvas.Pen.Style := psSolid;
  imgSourceImage.Picture.Bitmap.Canvas.TextOut(0, 0,
    UTF8Encode(Character));
  Extent:=imgSourceImage.Picture.Bitmap.Canvas.TextExtent(
    UTF8Encode(Character));
  //imgSourceImage.Refresh;
  //imgSourceImage.Repaint;
  //DrawZoom; // No! We draw the zoom in the Move procedure already!
  //lsCharacters.Repaint;
  MoveFlag := False;
  seWidth.Value:=Extent.cx;
  seHeight.Value:=Extent.cy;
  MoveFlag := True;
  Move; // Here we assign the InputPattern array and feed the network
  // Get a Unicode representation of the input pattern
  sInputPattern:=FormatNeuralFloatArray('0.000000000', InputPattern);
  // Test the InputPattern array
  WriteToLog('The input pattern is: ' + sInputPattern);
  // Get the guess before teaching and return it
  sGuess := GetNetworkGuess;
  WriteToLog(sGuess);
end;

procedure TfCOCR.cbTeachCharsToTeachOnlyChange(Sender: TObject);
begin
  if NetworkPrepared then
    Network.TeachCharsToTeachOnly:=cbTeachCharsToTeachOnly.Checked;
end;

procedure TfCOCR.SaveNetwork(SaveAs: Boolean);
var
  FilePath, FileDir: String;
  SaveFlag: Boolean;
  ShowDialog: Boolean;
  OrigControlsEnabled: Boolean;
begin
  if NetworkPrepared then
  begin
    OrigControlsEnabled := ControlsEnabled;
    ControlsEnabled := False;
    if SaveAs then
    begin
      ShowDialog := True;
    end
    else // Save only
    begin
      if NetworkFilePath > '' then
        ShowDialog := False
      else
        ShowDialog := True;
    end;
    if ShowDialog then
    begin
      sdSave.Filter := 'Neural network files (*.net)|*.net';
      if NetworkFilePath > '' then
      begin
        FileDir := ExtractFilePath(NetworkFilePath);
        if DirectoryExists(FileDir) then sdSave.InitialDir := FileDir;
        sdSave.FileName := NetworkFilePath;
      end;
      SaveFlag := sdSave.Execute;
      if SaveFlag then
        FilePath := sdSave.FileName;
    end
    else
    begin
      SaveFlag := True;
      FilePath := NetworkFilePath;
    end;
    try
      if SaveFlag then
      begin
        try
          Network.SaveYourself(FilePath);
          NetworkFilePath := FilePath; { Form caption and last network file path
                                         get updated }
          ClearLog;
          WriteToLog(
            Format(
              'The network has been saved to file “%s”.',
              [FilePath]
            )
          );
        except
          on E: Exception do
          begin
            WriteToLog(
              Format(
                'Error by saving the network file.%s' +
                '  Class:   %s%s' +
                '  Message: %s',
                [
                  LineEnding,
                  E.ClassName,
                  LineEnding,
                  //E.Message
                  E.ToString
                ]
              )
            );
          end;
        end;
      end;
    finally
      if ControlsEnabled <> OrigControlsEnabled then
        ControlsEnabled := OrigControlsEnabled;
    end;
  end
  else
  begin
    ShowMessage('The network isn’t prepared yet!');
  end;
end;

procedure TfCOCR.LoadSourceImage(FileName: String);
var
  FS: TFileStream;
  MS: TMemoryStream;
begin
  if FileExists(FileName) then
  begin
    { To avoid writing to disk, we need to load the picture
    from a memory stream }
    try
      FS := TFileStream.Create(FileName, fmOpenRead);
      MS := TMemoryStream.Create;
      MS.CopyFrom(FS, FS.Size);
      MS.Position := 0;
      imgSourceImage.Picture.LoadFromStreamWithFileExt(MS, 'png');
      //imgSourceImage.Picture.LoadFromFile(FileName);
    finally
      FS.Free;
      MS.Free;
    end;
  end;
  seWidth.MaxValue := imgSourceImage.Width;
  seHeight.MaxValue := imgSourceImage.Height;
  seLeft.MaxValue := imgSourceImage.Width - seWidth.Value;
  seTop.MaxValue := imgSourceImage.Height - seHeight.Value;
end;

procedure TfCOCR.SetControlsEnabled(Enable: Boolean);
begin
  // Skip disabling the controls if the form is minimized
  // (experimental)
  if (Self.WindowState <> wsMinimized) or (Enable = True) then
  begin
    btLoadSourceImage.Enabled:=Enable;
    btTestOneChar.Enabled:=Enable;
    seLeft.Enabled:=Enable;
    seTop.Enabled:=Enable;
    seWidth.Enabled:=Enable;
    seHeight.Enabled:=Enable;
    arLeft.Enabled:=Enable;
    arUp.Enabled:=Enable;
    arRight.Enabled:=Enable;
    arDown.Enabled:=Enable;
    edCorrection.Enabled:=Enable;
    if not CorrectFlag then
      tbCorrect.Enabled:=Enable;
    imgSourceImage.Enabled:=Enable;
    lsCharacters.Enabled:=Enable;
    btYes.Enabled:=Enable;
    btNo.Enabled:=Enable;
    if not tbTeachTheNetwork.Checked then
      tbTeachTheNetwork.Enabled:=Enable;
    tbNew.Enabled:=Enable;
    tbOpen.Enabled:=Enable;
    tbSave.Enabled:=Enable;
    tbSaveAs.Enabled:=Enable;
  end;
  FControlsEnabled:=Enable;
end;

function TfCOCR.GetCharsToTeach: UnicodeString;
begin
  Result := UTF8Decode(edCharsToTeach.Text);
end;

procedure TfCOCR.SetCharsToTeach(TheChars: UnicodeString);
begin
  edCharsToTeach.Text := UTF8Encode(TheChars);
end;

procedure TfCOCR.CreateSettings;
begin
  XMLC.StartEmpty:=True;
  XMLC.Filename:=FConfigFilePath;
  SaveSettings;
end;

procedure TfCOCR.LoadSettings;
var
  Answer: TModalResult;
const
  DlgQuestion =
    'There was an error while reading the configuration file.' + LineEnding +
    'Should I create it anew?';
  DlgInfo =
    'You chose not to recreate the configuration file.' + LineEnding +
    'Please repair the “%s” file manually or delete it.' + LineEnding +
    'The program will exit after a while.';
begin
  if FileExistsUTF8(FConfigFilePath) then
  begin
    XMLC.StartEmpty:=False;
    try
      XMLC.Filename:=FConfigFilePath;
    except
      on E: EXMLReadError do
      begin
        Answer :=
          MessageDlg(Application.Name, DlgQuestion, mtError, mbYesNo, 0);
        if Answer = mrYes then
          CreateSettings
        else
        begin
          ShowMessageFmt(DlgInfo, [FConfigFilePath]);
          Application.Terminate;
          Exit;
        end;
      end;
    end;
  end
  else
  begin
    XMLC.StartEmpty:=True;
    XMLC.Filename:=FConfigFilePath;
  end;
  CharsToTeach := XMLC.GetValue(txtTextToTeach, DefaultTextToTeach);
  TeachCharsToTeachOnly := XMLC.GetValue(txtTeachCharsToTeachOnly, False);
  LastNetworkFilePath := UTF8Encode(XMLC.GetValue(txtLastNetworkFilePath, ''));
end;

procedure TfCOCR.SaveSettings;
begin
  XMLC.SetValue(txtTextToTeach, CharsToTeach);
  XMLC.SetValue(txtTeachCharsToTeachOnly, TeachCharsToTeachOnly);
  XMLC.SetValue(txtLastNetworkFilePath, UTF8Decode(LastNetworkFilePath));
  XMLC.Flush;
end;

procedure TfCOCR.ShowNetworkProperties;
begin
  if Network <> nil then
  begin
    with PropEditor do
    begin
      Values[txtLearningRate] := FloatToStr(Network.LEARNING_RATE);
      //Values[txtSigmoidSlope] := FloatToStr(SigmoidSlope);
      //Values[txtOutputThreshold] := FloatToStr(Network.OUTPUT_THRESHOLD);
      Values[txtEpochCount] := IntToStr(Network.EpochCount);
      Values[txtAbsoluteMatchAchieved] :=
        BoolToStr(Network.AbsoluteMatchAchieved, True);
      Update;
    end;
    edCharsToTeach.Text:=Network.CharsToTeach;
    cbTeachCharsToTeachOnly.Checked:=Network.TeachCharsToTeachOnly;
  end;
end;

procedure TfCOCR.WriteToLog(Message: String);
var
  sMessage: String;
begin
  sMessage:=UTF8Encode(Message);
  mmLog.Append(sMessage);
  mmLog.SelStart:=Length(mmLog.Lines.Text);
end;

procedure TfCOCR.ClearLog;
begin
  mmLog.Clear;
end;

procedure TfCOCR.SaveLog;
var
  LogPath: String;
  SaveFlag: Boolean;
begin
  sdSave.Filter:='Log files (*.log)|*.log';
  SaveFlag := sdSave.Execute;
  if SaveFlag then
  begin
    LogPath := sdSave.FileName;
    mmLog.Lines.SaveToFile(LogPath);
  end;
end;

procedure TfCOCR.RefreshCaption;
  function NonemptyFileName(FilePath: String): String;
  begin
    if FilePath > '' then
      Result := ExtractFileName(FilePath)
    else
      Result := '[new]';
  end;
  function ModifiedString: String;
  begin
    if NetworkPrepared and Network.Modified then
      Result := ' *'
    else
      Result := '';
  end;
var
  sFileName, sModified: String;
begin
  sFileName := NonemptyFileName(FNetworkFilePath);
  sModified := ModifiedString();
  Caption := 'COCR ‐ ' + sFileName + sModified;
end;

function TfCOCR.GetCursorRect: TRect;
begin
  Result := Rect(seLeft.Value, seTop.Value, seLeft.Value +
    seWidth.Value, seTop.Value + seHeight.Value);
end;

procedure TfCOCR.SetCursorRect(TheCursor: TRect);
begin
  seLeft.Value := TheCursor.Left;
  seTop.Value := TheCursor.Top;
  seWidth.Value := TheCursor.Right - TheCursor.Left;
  seHeight.Value := TheCursor.Bottom - TheCursor.Top;
end;

procedure TfCOCR.FillCharacterList;
var
  I: Longint;
begin
  for I := 0 to HighestChar do
  begin
    lsCharacters.Items.Add(IntToStr(I) + ' ' +
      UTF8Encode(UnicodeString(UnicodeChar(I))));
  end;
end;

procedure TfCOCR.Move;
begin
  if MoveFlag then
  begin
    //imgSourceImage.Refresh;
    imgSourceImage.Repaint;
    DrawZoom;
    bevCursor.Left := CursorRect.Left;
    bevCursor.Top := CursorRect.Top;
    bevCursor.Width := CursorRect.Right - CursorRect.Left;
    bevCursor.Height := CursorRect.Bottom - CursorRect.Top;
    if not NetworkPrepared then
      Exit;
    // Feed the network
    InputPattern := GetInputPattern;
    Network.feed_forward(InputPattern);
    //lsCharacters.Refresh;
    //lsCharacters.Repaint;
    RefreshCharts;
    // This shouldn't take so much time as in the past versions.
    lblGuess.Caption := GetNetworkGuess;
    lblError.Caption := GetNetworkError;
  end;
end;

procedure TfCOCR.RefreshCharts;
begin
  //DrawZoom;
  //imgSynapses.Refresh;
  imgSynapses.Repaint;
  //imgBits.Refresh;
  imgBits.Repaint;
end;

function TfCOCR.GetGlyphRect: TRect;
var
  X, Y: Integer;
  PixelColor: TColor;
  Found: Boolean;
  CR: TRect;
  SrcCanvas: TCanvas;
const
  ThresholdColor: TColor = $F5F5F5;
begin
  // Initialization to the cursor rectangle (for the case of empty space)
  CR := CursorRect;
  Result := CR;
  //SrcCanvas := imgSourceImage.Picture.Bitmap.Canvas;
  SrcCanvas := imgSourceImage.Canvas;
  // Top glyph boundary
  Found := False;
  for Y := CR.Top to CR.Bottom - 1 do
  begin
    for X := CR.Left to CR.Right - 1 do
    begin
      PixelColor := SrcCanvas.Pixels[X, Y];
      if PixelColor < ThresholdColor then
      begin
        Result.Top := Y;
        Found := True;
        Break;
      end;
    end;
    if Found then Break;
  end;
  // Left glyph boundary
  Found := False;
  for X := CR.Left to CR.Right - 1 do
  begin
    for Y := CR.Top to CR.Bottom - 1 do
    begin
      PixelColor := SrcCanvas.Pixels[X, Y];
      if PixelColor < ThresholdColor then
      begin
        Result.Left := X;
        Found := True;
        Break;
      end;
    end;
    if Found then Break;
  end;
  // Bottom glyph boundary
  Found := False;
  for Y := CR.Bottom - 1 downto CR.Top do
  begin
    for X := CR.Left to CR.Right - 1 do
    begin
      PixelColor := SrcCanvas.Pixels[X, Y];
      if PixelColor < ThresholdColor then
      begin
        Result.Bottom := Y + 1;
        Found := True;
        Break;
      end;
    end;
    if Found then Break;
  end;
  // Right glyph boundary
  Found := False;
  for X := CR.Right - 1 downto CR.Left do
  begin
    for Y := CR.Top to CR.Bottom - 1 do
    begin
      PixelColor := SrcCanvas.Pixels[X, Y];
      if PixelColor < ThresholdColor then
      begin
        Result.Right := X + 1;
        Found := True;
        Break;
      end;
    end;
    if Found then Break;
  end;
end;

procedure TfCOCR.DrawZoom;
var
  ZoomWidth: Integer;
  ZoomHeight: Integer;
  SourceRect: TRect;
  SourceSize: TSize;
  //ZoomRatio: Double;
begin
  DrawCharRect := False;
  //imgSourceImage.Refresh;
  //imgSourceImage.Repaint;
  SourceRect := GetGlyphRect;
  SourceSize := Size(SourceRect);
  if SourceSize.cy > SourceSize.cx then
  begin
    ZoomWidth :=
      MathRound(SourceSize.cx / SourceSize.cy * imgZoom.Height);
    ZoomHeight := imgZoom.Height;
  end
  else if SourceSize.cy < SourceSize.cx then
  begin
    ZoomWidth := imgZoom.Width;
    ZoomHeight :=
      MathRound(SourceSize.cy / SourceSize.cx * imgZoom.Width);
  end
  else // if SourceSize.Height = SourceSize.Width (square)
  begin
    ZoomWidth := imgZoom.Width;
    ZoomHeight := imgZoom.Height;
  end;
  // White background on imgZoom
  imgZoom.Picture.Bitmap.Canvas.Pen.Color := imgZoom.Canvas.Brush.Color;
  imgZoom.Picture.Bitmap.Canvas.Rectangle(0, 0, imgZoom.Width, imgZoom.Height);
  // Draw the glyph
  imgZoom.Picture.Bitmap.Canvas.CopyRect(Rect(0, 0, ZoomWidth, ZoomHeight),
    imgSourceImage.Picture.Bitmap.Canvas, SourceRect);
  //imgZoom.Refresh;
  imgZoom.Repaint;
  DrawCharRect := True;
  //imgSourceImage.Repaint; // Draw the character rectangle
end;

procedure TfCOCR.RefreshCorrection;
var Letter: UnicodeChar;
begin
  Letter:=UnicodeChar(lsCharacters.ItemIndex);
  edCorrection.Text := UTF8Encode(UnicodeString(Letter));
  RefreshCharts;
end;

function TfCOCR.GetInputPattern: TNeuralFLoatArray;
var
  N, X, Y: Integer;
  PixelColor: TColor;
begin
  Result := TNeuralFloatArray.Create;
  SetLength(Result, 100);
  for N := 0 to 99 do
  begin
    X := N mod 10;
    Y := N div 10;
    {PixelColor := imgSourceImage.Picture.Bitmap.Canvas.Pixels[Cursor.Left + X,
      Cursor.Top + Y];}
    PixelColor := imgZoom.Picture.Bitmap.Canvas.Pixels[X * 4, Y * 4];
    {Network.InputLayer[N].Input :=
      (((Red(PixelColor) +
      Green(PixelColor) +
      Blue(PixelColor)) / 3) / $FF) * 2 - 1;}
    Result[N] :=
      1 - (((Red(PixelColor) +
      Green(PixelColor) +
      Blue(PixelColor)) / 3) / $FF);
  end;
end;

function TfCOCR.GetNetworkGuess: UTF8String;
var
  CharCode: Word;
begin
  if not NetworkPrepared then
  begin
    Result := '';
    Exit;
  end;
  CharCode := COCRUtils.NetworkOutputToCharCode(Network);
  Result := UTF8Encode(UnicodeString(UnicodeChar(CharCode)));
end;

{function TfCOCR.GetNetworksOpinionAboutCharacter(Character: UnicodeChar):
  Extended;
var
  Kod: Word absolute Character;
begin
  Result := Network.OutputLayer.Items[Kod].Value;
end;}

function TfCOCR.GetCorrectionCharacter: UnicodeChar;
var
  UTF8Text: UTF8String;
  WideText: UnicodeString;
begin
  UTF8Text:=edCorrection.Text;
  WideText:=UTF8Decode(UTF8Text);
  if WideText > '' then
    Result:=WideText[1]
  else
    Result:=#0;
end;

function TfCOCR.GetNetworkError: String;
var
  //extError: Extended;
  sngError: Double;
begin
  if Network <> nil then
    //extError := Network.AvgError
    sngError := Network.calculate_total_error(
      TTrainingSets.Create(
        CreateTrainingSet(InputPattern, TargetOutputPattern)
      )
    )
  else
    //extError:= 0;
    sngError := 0;
  //Result := FloatToStr(extError);
  Result := FloatToStr(sngError);
end;

{function TfCOCR.GetNetworksOpinionAboutCorrectionCharacter: Extended;
var SelChar: UnicodeChar;
begin
  SelChar:=GetCorrectionCharacter;
  Result:=GetNetworksOpinionAboutCharacter(SelChar);
end;}

procedure TfCOCR.CreateNetwork;
var
  NumberOfHiddenNeurons: Integer;
begin
  ControlsEnabled:=False;
  NetworkPrepared:=False;
  if Network <> nil then
    FreeAndNil(Network);
  // We create the network
  try
    //Network := TNeuralNetwork.Create([100, 256 {8} {12} {16}, BitCount]);
    {NumberOfHiddenNeurons :=
      GetOptimalNumberOfHiddenNeurons(100, BitCount, Length(CharsToTeach) * 1000,
      2);}
    //NumberOfHiddenNeurons := (100 + BitCount) div 2;
    NumberOfHiddenNeurons := QueryNumberOfHiddenNeurons;
    if (NumberOfHiddenNeurons < 2) then
      ShowMessage('The number of the hidden neurons must be two or more.')
    else
    begin
      Network := TNeuralNetwork.Create(100, NumberOfHiddenNeurons, BitCount,
        [], 1, [], 1);
      Network.OnModifiedChange := @NetworkModifiedChange;
      NetworkFilePath := '';
      NetworkPrepared := True;
      lsCharacters.Repaint;
      RefreshCharts;
      ShowNetworkProperties;
      mmLog.Append(
        Format(
          'A new network has been created. Layout is: %d inputs, ' +
          '%d hidden neurons, and %d outputs.',
          [
            Network.num_inputs,
            Network.HiddenLayer.Count,
            Network.OutputLayer.Count
          ]
        )
      );
      Beep;
    end;
  finally
    ControlsEnabled := True;
  end;
end;

procedure TfCOCR.LoadNetwork;
var
  FilePath, FileDir: String;
  LoadFlag: Boolean;
begin
  ControlsEnabled:=False;
  //FilePath := OurPath('network.net');
  odOpen.Filter := 'Neural network files (*.net)|*.net';
  odOpen.Options := [ofPathMustExist, ofFileMustExist];
  if NetworkFilePath > '' then
  begin
    FileDir := ExtractFilePath(LastNetworkFilePath);
    if DirectoryExists(FileDir) then odOpen.InitialDir := FileDir;
    odOpen.FileName := LastNetworkFilePath;
  end;
  LoadFlag := odOpen.Execute;
  try
    if LoadFlag then
    begin
      FilePath := odOpen.FileName;
      NetworkPrepared:=False;
      FreeAndNil(Network);
      LoadNetworkFromFile(FilePath);
    end;
  finally
    ControlsEnabled := True;
  end;
end;

procedure TfCOCR.LoadNetworkFromFile(FilePath: String);
begin
  Network := TNeuralNetwork.Create(FilePath);
  NetworkFilePath := FilePath; { Form caption and last network file path
                                 get updated }
  ShowNetworkProperties;
  Network.OnModifiedChange := @NetworkModifiedChange;
  NetworkPrepared:=True;
  mmLog.Append(Format('The network has been loaded from file “%s”.',
    [FilePath]));
end;



procedure TfCOCR.TrainNetwork;
var
  CorrectionWCh: UnicodeChar;
  CorrectionCode: Word;
  //Err: Extended;
  //TargetOutputPattern: {TNeuralFLoatArray} TDoubleArray; // moved to form fields
begin
  if not NetworkPrepared then
    Exit;

  CorrectionWCh := GetCorrectionCharacter;
  CorrectionCode := Word(CorrectionWCh);

  lblGuess.Caption := GetNetworkGuess;

  //pbProgress.Min := Low(Word);
  //pbProgress.Max := High(Word);

  TargetOutputPattern := CharCodeToTargetNetworkOutput(CorrectionCode);
  //Network.Evaluate(TargetOutputPattern);
  // InputPattern was already assigned in procedure Move
  //InputPattern := GetInputPattern;
  Network.train(InputPattern, TargetOutputPattern);
  RefreshCharts;
  //lsCharacters.Refresh;
  //lsCharacters.Repaint;
  //pbProgress.Position:=CorrectionCode;
  Application.ProcessMessages;

  lblGuess.Caption := GetNetworkGuess;
  lblError.Caption := GetNetworkError;
  //lsCharacters.Repaint;
  RefreshCharts;
end;

procedure TfCOCR.FormCreate(Sender: TObject);
var
  //I, J: word;
  BMP: TBitmap;
  sLastNFP: String;
const
  MONOSPACE =
    {$IFDEF WINDOWS}
      'Courier New'
    {$ELSE}
      'Monospace'
    {$ENDIF};
  FONT_SIZE = 10;
begin
  //LeftMouseButtonDepressed:=False;
  NetworkPrepared := False;
  DrawCharRect := True;
  FControlsEnabled := True;
  // We load the README text
  mmCOCR.Lines.LoadFromFile(OurPath('README.md'));
  // We set the README font
  mmCOCR.Font.Name := MONOSPACE;
  mmCOCR.Font.Size := FONT_SIZE;
  // We set the log font
  mmLog.Font.Name := MONOSPACE;
  mmLog.Font.Size := FONT_SIZE;
  // We get the config file path
  FConfigFilePath:=GetAppConfigFileUTF8(False, True, True);
  // We load the settings
  LoadSettings;
  // We fill the activation list
  FillCharacterList;
  // We create bitmaps as grounds for drawing:
  BMP := TBitmap.Create;
  BMP.Width := imgZoom.Width;
  BMP.Height := imgZoom.Height;
  imgZoom.Picture.Bitmap := BMP;
  BMP := TBitmap.Create;
  BMP.Width := imgSynapses.Width;
  BMP.Height := imgSynapses.Height;
  imgSynapses.Picture.Bitmap := BMP;
  BMP := TBitmap.Create;
  BMP.Width := imgBits.Width;
  BMP.Height := imgBits.Height;
  imgBits.Picture.Bitmap := BMP;
  // We load the default picture:
  LoadSourceImage(DefaultSourceImageFileName);
  MoveFlag := True;
  Move;
  // We initialise the InputPattern graph neuron index iterator to 0:
  HiddenNeuronToDrawIndex:=0;
  // Initialise the network property grid
  PropEditor.ColWidths[0] := 140;
  with PropEditor.Strings do
  begin
    Clear;
    Append(txtLearningRate + '=' + FloatToStr(DefaultLearningRate));
    //Append(txtSigmoidSlope + '=' + FloatToStr(DefaultSigmoidSlope));
    //Append(txtOutputThreshold + '=' + FloatToStr(DefaultOutputThreshold));
    Append(txtEpochCount + '=0');
    Append(txtAbsoluteMatchAchieved + '=False');
  end;
  // And, finally, try to load the last used network if it exists.
  // Settings already loaded.
  sLastNFP := LastNetworkFilePath;
  if FileExists(sLastNFP) then
    LoadNetworkFromFile(sLastNFP);
end;

procedure TfCOCR.imgBitsClick(Sender: TObject);
begin
  //imgBits.Refresh;
  imgBits.Repaint;
end;

procedure TfCOCR.imgBitsPaint(Sender: TObject);
var
  X, Y, N, R, G, B: integer;
  CurrentOutputs: TNeuralFLoatArray;
begin
  if not NetworkPrepared then
    exit;

  CurrentOutputs := Network.OutputLayer.get_outputs();

  for N := 0 to High(CurrentOutputs) do
  begin
    X := 15 - N;
    Y := 0;
    //R := Trunc((Network.OutputLayer[N].Value + 1) / 2 * 255);
    R := trunc(CurrentOutputs[N] * 255);
    G := R; //Trunc((Network.OutputLayer[N].Value + 1) / 2 * 255);
    B := R;
    imgBits.Canvas.Brush.Color := RGBToColor(R, G, B);
    imgBits.Canvas.Brush.Style := bsSolid;
    imgBits.Canvas.Rectangle(X * 4, Y * 4, (X + 1) * 4 + 1, (Y + 1) * 4 + 1);
  end;
end;

procedure TfCOCR.edCorrectionChange(Sender: TObject);
begin

end;

procedure TfCOCR.bevCursorChangeBounds(Sender: TObject);
begin

end;

procedure TfCOCR.edCorrectionKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #13 then
  begin
    CorrectFlag:=True;
    Correct;
  end;
end;

procedure TfCOCR.edCharsToTeachChange(Sender: TObject);
begin
  if NetworkPrepared then
    Network.CharsToTeach:=edCharsToTeach.Text;
end;

procedure TfCOCR.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  SaveSettings;
  FreeAndNil(Network);
end;

procedure TfCOCR.FormCloseQuery(Sender: TObject; var CanClose: boolean);
var
  Resp: TModalResult;
begin
  if NetworkPrepared and Network.Modified then
  begin
    Resp := MessageDlg(
      'COCR', 'Do you want to save the network?', mtConfirmation, mbYesNoCancel,
      0
    );
    case Resp of
    mrYes:
      begin
        SaveNetwork(False);
        CanClose:=True;
      end;
    mrNo:
      begin
        CanClose:=True;
      end;
    else //mrCancel
      begin
        CanClose:=False;
      end;
    end;
  end;
end;

procedure TfCOCR.imgSourceImageMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
var X0, Y0: Integer;
begin
  X0 := X - seWidth.Value div 2;
  Y0 := Y - seHeight.Value div 2;
  if (X0 > 0) and (X0 < imgSourceImage.Width - 1 - seWidth.Value) then
    seLeft.Value := X0;
  if (Y0 > 0) and (Y0 < imgSourceImage.Height - 1 - seHeight.Value) then
    seTop.Value := Y0;
  if Button = mbLeft then
  begin
    LeftMouseButtonDepressed := True;
    Move;
  end;
end;


procedure TfCOCR.imgSourceImageMouseMove(Sender: TObject; Shift: TShiftState;
  X, Y: integer);
var X0, Y0: Integer;
begin
  if LeftMouseButtonDepressed then
  begin
    X0 := X - seWidth.Value div 2;
    Y0 := Y - seHeight.Value div 2;
    if (X0 > 0) and (X0 < imgSourceImage.Width - 1 - seWidth.Value) then
      seLeft.Value := X0;
    if (Y0 > 0) and (Y0 < imgSourceImage.Height - 1 - seHeight.Value) then
      seTop.Value := Y0;
    Move;
  end;
end;

procedure TfCOCR.imgSourceImageMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: integer);
var X0, Y0: Integer;
begin
  X0 := X - seWidth.Value div 2;
  Y0 := Y - seHeight.Value div 2;
  if (X0 > 0) and (X0 < imgSourceImage.Width - 1 - seWidth.Value) then
    seLeft.Value := X0;
  if (Y0 > 0) and (Y0 < imgSourceImage.Height - 1 - seHeight.Value) then
    seTop.Value := Y0;
  Move;
  if Button = mbLeft then
    LeftMouseButtonDepressed := False;
end;

procedure TfCOCR.imgSourceImagePaint(Sender: TObject);
var
  Obd: TRect;
begin
  if DrawCharRect then
  begin
    Obd := CursorRect;
    with Obd do
    begin
      Left -= 1;
      Top -= 1;
      Right += 1;
      Bottom += 1;
    end;
    imgSourceImage.Canvas.Brush.Style := bsClear;
    imgSourceImage.Canvas.Rectangle(Obd);
  end;
end;

procedure TfCOCR.imgSynapsesClick(Sender: TObject);
var
  {Message: String;
  Neuron: TNeuron;}
  NeuronCount: Integer;
begin
  if NetworkPrepared then
  begin
    //NeuronCount := Network.HiddenLayers[0].Count;
    NeuronCount := Network.HiddenLayer.Count;
    Inc(HiddenNeuronToDrawIndex);
    if HiddenNeuronToDrawIndex = NeuronCount then
      HiddenNeuronToDrawIndex:=0;
    {
    Neuron := Network.InputLayer[0];
    Message:= 'Synapse counts: ' + IntToStr(Length(Neuron.SynapseArray));
    Neuron := Network.HiddenLayers[0][0];
    Message += ', ' + IntToStr(Length(Neuron.SynapseArray));
    Neuron := Network.OutputLayer[0];
    Message += ', ' + IntToStr(Length(Neuron.SynapseArray));
    ShowMessage(Message);
    }
  end;

  RefreshCharts;
end;

{.$DEFINE INPUTS}
procedure TfCOCR.imgSynapsesPaint(Sender: TObject);
var
  X, Y, {BW,} R, G, B: Integer;
  {$IFNDEF INPUTS}
  MinWeight, MaxWeight, W, WeightRange, FromMinWeight: Extended;
  S{ynapse}{, K}: Integer;
  {Synapse: TSynapse;}
  objN: TNeuron;
  {$ELSE}
  N: Integer;
  I: Extended;
  {$ENDIF}
begin
  if not NetworkPrepared then
    Exit;

  {$IFNDEF INPUTS}
  //objN := Network.HiddenLayers[0].Items[HiddenNeuronToDrawIndex];
  objN := Network.HiddenLayer.Items[HiddenNeuronToDrawIndex];
  MaxWeight := objN.GetMaxWeight;
  MinWeight := objN.GetMinWeight;
  WeightRange := MaxWeight - MinWeight;

  for S := 0 to High(objN.weights) do
  begin
    X := S mod 10;
    Y := S div 10;

    {Synapse:=objN.BackwardSynapses[S];
    W:=Synapse.Weight;}
    W := objN.weights[S];

    FromMinWeight := W - MinWeight;
    if WeightRange <> 0 then
      R := Trunc((FromMinWeight / WeightRange) * Extended(255))
    else // WeightRange = 0
      //R := Trunc(((MinWeight + 1) / 2) * Extended(255));
      R := trunc(0.5 * 255);
    G := R;
    B := R;
    imgSynapses.Canvas.Brush.Color := RGBToColor(R, G, B);
    imgSynapses.Canvas.Brush.Style := bsSolid;
    imgSynapses.Canvas.Rectangle(X * 4, Y * 4, (X + 1) * 4 + 1, (Y + 1) * 4 + 1);
  end;

  {$ELSE}

  InputPattern := GetInputPattern;
  //for N := 0 to Network.InputLayer.Count - 1 do
  for N := 0 to High(InputPattern) do
  begin
    X := N mod 10;
    Y := N div 10;

    {Neuron:=Network.InputLayer.Items[N];
    I:=Neuron.GetInput;}
    I := InputPattern[N];

    //R:=Trunc((I + 1) / 2 * 255);
    R := trunc(I * 255);
    G := R;
    B := R;
    imgSynapses.Canvas.Brush.Color := RGBToColor(R, G, B);
    imgSynapses.Canvas.Brush.Style := bsSolid;
    imgSynapses.Canvas.Rectangle(X * 4, Y * 4, (X + 1) * 4 + 1, (Y + 1) * 4 + 1);
  end;

  {$ENDIF}

end;

procedure TfCOCR.lblCharsToTeachClick(Sender: TObject);
begin

end;

procedure TfCOCR.PropEditorHeaderSized(Sender: TObject; IsColumn: Boolean;
  Index: Integer);
begin
  if IsColumn and (Index = 0) then
    mmLog.Append('Property column width: ' + IntToStr(PropEditor.ColWidths[0]));
end;

procedure TfCOCR.PropEditorValidateEntry(sender: TObject; aCol, aRow: Integer;
  const OldValue: string; var NewValue: String);
var
  KeyName: String;
begin
  if aCol = 1 then
  begin
    KeyName := PropEditor.Strings.Names[aRow - PropEditor.FixedRows];
    try
      if KeyName = txtLearningRate then
      begin
        if Network <> nil then
        begin
          Network.LearningRate := StrToFloat(NewValue);
          NewValue := FloatToStr(Network.LearningRate);
        end;
      end
      {else if KeyName = txtSigmoidSlope then
      begin
        SigmoidSlope := StrToFloat(NewValue);
        NewValue := FloatToStr(SigmoidSlope);
      end}
      {else if KeyName = txtOutputThreshold then
      begin
        if Network <> nil then
        begin
          Network.OutputThreshold:=StrToFloat(NewValue);
          NewValue := FloatToStr(Network.OutputThreshold);
        end;
      end}
      else if KeyName = txtEpochCount then
        NewValue:=OldValue
      else if KeyName = txtAbsoluteMatchAchieved then
        NewValue:=OldValue;
    except
      on E: Exception do
      begin
        ShowMessage('An error of class ' + E.ClassName + ' has occured.' +
          LineEnding +
          'Message: “' + E.Message + '”');
        NewValue := OldValue;
      end;
    end;
  end;
end;

procedure TfCOCR.seLeftChange(Sender: TObject);
begin
  Move;
end;

procedure TfCOCR.seTopChange(Sender: TObject);
begin
  Move;
end;

procedure TfCOCR.seWidthChange(Sender: TObject);
begin
  Move;
end;

procedure TfCOCR.seHeightChange(Sender: TObject);
begin
  Move;
end;

procedure TfCOCR.lsCharactersClick(Sender: TObject);
var Pjsmeno: UnicodeChar;
begin
  Pjsmeno:=UnicodeChar(lsCharacters.ItemIndex);
  edCorrection.Text := UTF8Encode(UnicodeString(Pjsmeno));
  RefreshCharts;
end;

procedure TfCOCR.lsCharactersDblClick(Sender: TObject);
begin
  edCorrection.Text := UTF8Encode(UnicodeString(UnicodeChar(lsCharacters.ItemIndex)));
  CorrectFlag:=True;
  Correct;
end;

procedure TfCOCR.lsCharactersDrawItem(Control: TWinControl; Index: integer;
  ARect: TRect; State: TOwnerDrawState);
var
  OriginalPenColor: TColor;
begin
  if lsCharacters.ItemVisible(Index) then
  begin
    begin
      with (Control as TListBox).Canvas do
      begin
        OriginalPenColor:=Pen.Color;
        Pen.Color:=clWhite;
        Rectangle(ARect);
        Pen.Color:=OriginalPenColor;
        TextOut(ARect.Left + 1, ARect.Top + 1,
          (Control as TListBox).Items[Index]);
      end;
    end;
  end;
end;

procedure TfCOCR.lsCharactersKeyPress(Sender: TObject; var Key: char);
begin
  if Key = #$D then
  begin
    edCorrection.Text := UTF8Encode(UnicodeString(UnicodeChar(lsCharacters.ItemIndex)));
    TrainNetwork;
  end
end;

procedure TfCOCR.lsCharactersUTF8KeyPress(Sender: TObject; var UTF8Key: TUTF8Char
  );
var
  WCh: UnicodeChar;
begin
  WCh:=UTF8Decode(UTF8Key)[1];
  lsCharacters.ItemIndex:=Integer(WCh);
  RefreshCorrection;
end;

procedure TfCOCR.arLeftClick(Sender: TObject);
begin
  seLeft.Value:=seLeft.Value-1;
end;

procedure TfCOCR.arDownClick(Sender: TObject);
begin
  seTop.Value:=seTop.Value+1;
end;

procedure TfCOCR.arRightClick(Sender: TObject);
begin
  seLeft.Value:=seLeft.Value+1;
end;

procedure TfCOCR.arUpClick(Sender: TObject);
begin
  seTop.Value:=seTop.Value-1;
end;

procedure TfCOCR.tbCorrectChange(Sender: TObject);
begin
  if tbCorrect.Checked then
  begin
    CorrectFlag:=True;
    Correct;
    tbCorrect.Checked := CorrectFlag;
  end;
end;

procedure TfCOCR.tbNewClick(Sender: TObject);
begin
  CreateNetwork;
end;

procedure TfCOCR.tbNewLogClick(Sender: TObject);
begin
  mmLog.Clear;
end;

procedure TfCOCR.tbOpenClick(Sender: TObject);
begin
  LoadNetwork;
end;

procedure TfCOCR.tbSaveAsClick(Sender: TObject);
begin
  SaveNetwork(True);
end;

procedure TfCOCR.tbSaveClick(Sender: TObject);
begin
  SaveNetwork(False);
end;

procedure TfCOCR.tbSaveLogClick(Sender: TObject);
begin
  SaveLog;
end;

function TfCOCR.TestOneChar(Character: UnicodeChar): UnicodeChar;
var
  Extent: TSize;
  Guess: UTF8String;
  wGuess: UnicodeString;
begin
  //imgSourceImage.Picture.Bitmap.Canvas.Pen.Style := psClear;
  //imgSourceImage.Picture.Bitmap.Canvas.Rectangle(0, 0, 11, 11);
  imgSourceImage.Picture.Bitmap.Canvas.Pen.Style := psSolid;
  imgSourceImage.Picture.Bitmap.Canvas.TextOut(0, 0,
    UTF8Encode(Character));
  Extent:=imgSourceImage.Picture.Bitmap.Canvas.TextExtent(
    UTF8Encode(Character));
  //imgSourceImage.Refresh;
  //imgSourceImage.Repaint;
  //DrawZoom; // No! We draw the zoom in the Move procedure already!
  //lsCharacters.Repaint;
  MoveFlag := False;
  seWidth.Value:=Extent.cx;
  seHeight.Value:=Extent.cy;
  MoveFlag := True;
  Move; // Here we assign the InputPattern array and feed the network
  // Get the guess before teaching and return it
  Guess := GetNetworkGuess;
  wGuess := UTF8Decode(Guess);
  if Length(wGuess) > 0 then
    Result := wGuess[1]
  else
    Result := #0;
end;

function TfCOCR.CorrectOneChar(Character: UnicodeChar): UnicodeChar;
var
  Guess: UTF8String;
  wGuess: UnicodeString;
begin
  edCorrection.Text := UTF8Encode(Character);
  CorrectFlag:=True;
  Correct;
  // Get the guess after teaching and return it
  Guess := GetNetworkGuess;
  wGuess := UTF8Decode(Guess);
  if Length(wGuess) > 0 then
    Result := wGuess[1]
  else
    Result := #0;
end;

function TfCOCR.TeachOneChar(Character: UnicodeChar): TTeachingResult;
begin
  Result[trkBeforeTeaching] := TestOneChar(Character);
  Result[trkAfterTeaching] := CorrectOneChar(Character);
end;

function TfCOCR.QueryNumberOfHiddenNeurons: Integer;
var
  sInput: String;
begin
  sInput := InputBox('New network', 'Enter the number of the hidden neurons:',
    '256');
  try
    Result := StrToInt(sInput);
  except
    on EConvertError do
    begin
      Result := 0;
    end;
  end;
end;

procedure TfCOCR.Correct;
begin
  if CorrectFlag then
  begin
    TrainNetwork;
    Application.ProcessMessages;
    CorrectFlag:=False;
  end;
end;

procedure TfCOCR.tbTeachTheNetworkChange(Sender: TObject);
var
  I, J: Integer;
  Ch: UnicodeChar;
  Sum: Extended;
  EpochAvgError: Extended;
  TeachingResult: TTeachingResult;
  EpochResult: TEpochResult;
  locTextToTeach: UnicodeString;
  locTeachCharsToTeachOnly: Boolean;
  MinJ, MaxJ: Integer;
  CurrentEpoch, AeonStartEpoch, EpochsFromAeonStart: Integer;
  CurrentAeon: Integer;
  sCurrentEpoch: String;
const
  AeonLength = 1000000; // one million epochs
begin
  if tbTeachTheNetwork.Checked then
  begin
    if NetworkPrepared then
    begin
      if CharsToTeach > '' then
      begin
        // Disable controls
        ControlsEnabled:=False;
        // Move the source image cursor to the zero-point (upper-left corner)
        seLeft.Value := 0;
        seTop.Value := 0;
        // We're going to draw in a solid style
        imgSourceImage.Picture.Bitmap.Canvas.Brush.Style := bsSolid;

        // Compute the minimum and maximum epoch
        // Get the current epoch count (based on the network's saved value)
        CurrentEpoch:=Network.EpochCount;
        // Compute the whole aeon count reached until now
        CurrentAeon:=CurrentEpoch div AeonLength;
        // Compute the starting epoch of the current aeon
        AeonStartEpoch:=CurrentAeon * AeonLength;
        // Get the number of epochs from the start of the current aeon
        EpochsFromAeonStart:=CurrentEpoch - AeonStartEpoch;
        pbProgress.Step:=1;
        pbProgress.Min:=0;
        pbProgress.Max:=AeonLength;
        pbProgress.Position:=EpochsFromAeonStart;

        mmLog.Clear;
        for I := pbProgress.Position to pbProgress.Max - 1 do
        begin
          Sum := 0;
          locTextToTeach := RandomizeString(CharsToTeach);
          locTeachCharsToTeachOnly := TeachCharsToTeachOnly;
          if locTeachCharsToTeachOnly then
          begin
            MinJ := 1;
            MaxJ := Length(locTextToTeach);
          end
          else
          begin
            MinJ := Low(Word);
            MaxJ := High(Word);
          end;
          EpochResult := '';
          for J := MinJ to MaxJ do
          begin
            if locTeachCharsToTeachOnly then
              Ch := locTextToTeach[J]
            else
              Ch := UnicodeChar(J);
            TeachingResult := TeachOneChar(Ch);
            //Sum += Network.AvgError;
            Sum += Network.calculate_total_error(
              TTrainingSets.Create(
                CreateTrainingSet(InputPattern, TargetOutputPattern)
              )
            );
            EpochResult += TeachingResult;
            if not locTeachCharsToTeachOnly then
            begin
              mmLog.Append(
                UTF8Encode(
                  Ch +
                  TeachingResult[trkBeforeTeaching] +
                  TeachingResult[trkAfterTeaching]
                )
              );
              if (Ch = TeachingResult[trkBeforeTeaching]) and
                 (Ch = TeachingResult[trkAfterTeaching]) then
                 Beep;
            end;
            Application.ProcessMessages;
            if not tbTeachTheNetwork.Checked then
              Break;
          end;

          Network.EpochDone; // increment the network's internal epoch counter
          CurrentEpoch := Network.EpochCount;
          sCurrentEpoch:=IntToStr(CurrentEpoch);
          PropEditor.Values[txtEpochCount] := sCurrentEpoch;
          EpochAvgError := Sum / (MaxJ - MinJ + 1);
          if locTeachCharsToTeachOnly then
            mmLog.Append(
              'Epoch ' + sCurrentEpoch + ': ' +
              UTF8Encode(locTextToTeach) + ': ' +
              UTF8Encode(EpochResult) + ', ' +
              'error: ' + FloatToStr(EpochAvgError)
            );
          if EpochResult = locTextToTeach then
          begin
            Network.AbsoluteMatchAchieved:=True;
            PropEditor.Values[txtAbsoluteMatchAchieved] := 'True';
            mmLog.Append('Absolute match achieved. Learning done.');
            Break;
          end;

          lblEpochError.Caption := FloatToStr(EpochAvgError);

          //pbProgress.Position := CurrentEpoch;
          pbProgress.StepIt;

          // Save the network after a certain amount of epochs periodically
          if (I + 1) mod 1000 = 0 then
          begin
            if NetworkFilePath > '' {if the network file path is known} then
              SaveNetwork(False);
          end;

          if not tbTeachTheNetwork.Checked then
            Break;
        end;
        // Save the network on training end
        if NetworkFilePath > '' {if the network file path is known} then
          SaveNetwork(False);
        tbTeachTheNetwork.Checked := False;
        tbCorrect.Enabled:=True;
        ControlsEnabled:=True;
        Beep;
      end // if CharsToTeach > ''
      else
      begin
        MessageDlg('COCR', 'Please fill in some characters to teach.',
          mtInformation, [mbOK], 0, mbOK);
        tbTeachTheNetwork.Checked:=False;
      end;
    end // if NetworkPrepared
    else
    begin
      MessageDlg('COCR', 'The network hasnʼt been prepared yet!', mtWarning,
        [mbOK], 0, mbOK);
      tbTeachTheNetwork.Checked:=False;
    end;
  end; // if tbTeachTheNetwork.Checked
end;

procedure TfCOCR.btYesClick(Sender: TObject);
{var
  CorrectionText: UnicodeString;
  WCh: UnicodeChar;
  Kod: Word absolute WCh;
  N: TNeuron;
  //I: Integer;}
begin
{
  CorrectionText:=UTF8Decode(edCorrection.Text);
  if CorrectionText > '' then
    WCh:=CorrectionText[1]
  else
    WCh:=#0;
  N := Network.OutputLayer[Kod];
  //while N.Hodnota < 1 do
  //for I := 0 to 9 do
  begin
    N.Evaluate(1);
    RefreshCharts;
    Application.ProcessMessages;
  end;
  Move;
  //Beep;
}
end;

initialization
  {$I cocrmain.lrs}

end.

