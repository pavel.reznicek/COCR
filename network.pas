unit network;

{$mode objfpc}{$H+}
{$interfaces corba}

{$Define NEURON_DYNAMIC_VALUE} { A flag meaning that the neuron is computed
  dynamically on request or statically from the outside }

interface

uses
  Classes, SysUtils, {Contnrs,} Forms, Graphics, Variants, Neuron, fgl,
  Laz2_DOM, laz2_XMLRead, laz2_XMLWrite;
  
const
  DefaultLearningRate = 0.01;

  txtLearningRate = 'LearnigRate';
  txtSigmoidSlope = 'SigmoidSlope';

type

  //TChartKind = (ckFull, ckPoint);

  ENetworkError = class(Exception);

  TNeuralNetwork = class;

  TNeuronList = specialize TFPGObjectList<TNeuron>;

  { TNeuronLayer }

  TNeuronLayer = class(TNeuronList)
  private
    FLayerIndex: Integer;
    FNetwork: TNeuralNetwork;
    FKind: TNeuronKind;
    FConnected: Boolean;
    procedure SetLayerIndex(AValue: Integer);
    function GetAvgError: Extended;
    function GetNeuronCount: Integer;
  public
    constructor Create(ParentNetwork: TNeuralNetwork);
    constructor Create(ParentNetwork: TNeuralNetwork; LayerElement: TDOMElement;
      LayerKind: TNeuronKind);
    function Add(N: TNeuron): Integer;
    function Remove(N: TNeuron): Integer;
    function GetItem(Index: Integer): TNeuron;
    //procedure DrawSynapse(Output: TNeuron; S: TSynapse);
    function AddBackwardSynapse(Input, Output: TNeuron): TSynapse;
    function AddForwardSynapse(Input, Output: TNeuron): TSynapse;
    function GetDrawFlag: Boolean;
    //procedure PaintSynapses(Output: TNeuron = nil);
    //procedure OnSynapseChange(Sender: TObject; Syn: TSynapse);
    function GetMaxWeight: Extended;
    procedure ApplyAdjustedWeights;
    procedure SaveYourself(Doc: TXMLDocument; NetworkElement: TDOMElement);
    property Items[Index: Integer]: TNeuron read GetItem; default;
    property Neurons[Index: Integer]: TNeuron read GetItem;
    property AvgError: Extended read GetAvgError;
  published
    property Network: TNeuralNetwork read FNetwork;
    property DrawingEnabled: Boolean read GetDrawFlag;
    property LayerIndex: Integer read FLayerIndex write SetLayerIndex;
    property Kind: TNeuronKind read FKind;
    { We need to know if the layer is already connected via synapses to the
    previous layer's cells upon addition. So let's keep a flag indicating
    that. }
    property Connected: Boolean read FConnected write FConnected;
    property NeuronCount: Integer read GetNeuronCount;
  end;

  { TInputLayer }

  TInputLayer = class(TNeuronLayer)
  public
    constructor Create(ParentNetwork: TNeuralNetwork; ANeuronCount: Integer);
    constructor Create(ParentNetwork: TNeuralNetwork; LayerElement: TDOMElement);
    function Add(N: TInputNeuron): Integer;
    function Remove(N: TInputNeuron): Integer;
    function GetItem(Index: Integer): TInputNeuron;
    property Items[Index: Integer]: TInputNeuron read GetItem; default;
  end;

  { THiddenLayer }

  THiddenLayer = class(TNeuronLayer)
    constructor Create(ParentNework: TNeuralNetwork; ANeuronCount: Integer);
    constructor Create(ParenTNeuralNetwork: TNeuralNetwork; LayerElement: TDOMElement);
    function Add(N: THiddenNeuron): Integer;
    function Remove(N: THiddenNeuron): Integer;
    function GetItem(Index: Integer): THiddenNeuron;
    property Items[Index: Integer]: THiddenNeuron read GetItem; default;
  end;

  { TOutputLayer }

  TOutputLayer = class(TNeuronLayer)
    constructor Create(ParentNework: TNeuralNetwork; ANeuronCount: Integer);
    constructor Create(ParenTNeuralNetwork: TNeuralNetwork; LayerElement: TDOMElement);
    function Add(N: TOutputNeuron): Integer;
    function Remove(N: TOutputNeuron): Integer;
    function GetItem(Index: Integer): TOutputNeuron;
    property Items[Index: Integer]: TOutputNeuron read GetItem; default;
    procedure Evaluate(EstimatedOutput: array of Extended);
  end;

  { TNeuralNetwork }

  TLayerList = specialize TFPGObjectList<TNeuronLayer>;
  TNeuralNetwork = class(TLayerList)
  private
    FDrawFlag: Boolean;
    FLearningRate: Extended;
    //FChartKind: TChartKind;
    function GetLayer(Index: Integer): TNeuronLayer;
    function GetLayerCount: Integer;
    function GetInputLayer: TInputLayer;
    function GetHiddenLayer(Index: Integer): THiddenLayer;
    function GetHiddenLayerCount: Integer;
    function GetOutputLayer: TOutputLayer;
    function GetAvgError: Extended;
    procedure SetLearningRate(AValue: Extended);
  public
    constructor Create();
    constructor Create(LayerSizes: array of const);
    constructor Create(FilePath: String);
    function GetNeuron(LayerIndex, NeuronIndex: Integer): TNeuron;
    (*function AddNeuron(ItsLayer: Integer{; X: Integer = 0; Y: Integer = 0}):
      TNeuron;*)
    //procedure Paint; override;
    procedure Evaluate(DesiredOutput: array of Extended);
    procedure Backpropagate(Errors: array of Extended);
    procedure FacTabulamRasam; { create a “blank table” out of the network
      - chaotize connection weights - the network initializes and
      “forgets” what it has learned so far }
    //procedure WriteChartKind(Kind: TChartKind);
    function AddInputLayer(L: TInputLayer): Integer;
    function AddInputLayer(NeuronCount: Integer): TInputLayer;
    function AddHiddenLayer(L: THiddenLayer): Integer;
    function AddHiddenLayer(NeuronCount: Integer): THiddenLayer;
    function AddOutputLayer(L: TOutputLayer): Integer;
    function AddOutputLayer(NeuronCount: Integer): TOutputLayer;
    procedure ConnectLayers(InputLayer, OutputLayer: TNeuronLayer);
    procedure ApplyAdjustedWeights;
    procedure SaveYourself(Path: String);
    property Neurons[LayerIndex, NeuronIndex: Integer]: TNeuron read GetNeuron;
    property Layers[Index: Integer]: TNeuronLayer read GetLayer;
    property InputLayer: TInputLayer read GetInputLayer;
    property HiddenLayers[Index: Integer]: THiddenLayer read GetHiddenLayer;
    property HiddenLayerCount: Integer read GetHiddenLayerCount;
    property OutputLayer: TOutputLayer read GetOutputLayer;
    property LayerCount: Integer read GetLayerCount;
    property DrawFlag: Boolean read FDrawFlag write FDrawFlag;
    //property ChartKind: TChartKind read FChartKind write WriteChartKind;
    property AvgError: Extended read GetAvgError;
    property LearningRate: Extended read FLearningRate write SetLearningRate;
  end;

implementation

{ TOutputLayer }

constructor TOutputLayer.Create(ParentNework: TNeuralNetwork; ANeuronCount: Integer);
var
  N: Integer;
  objN: TOutputNeuron;
begin
  inherited Create(ParentNework);
  FKind := nkOutput;
  for N := 0 to ANeuronCount - 1 do
  begin
    objN := TOutputNeuron.Create;
    Add(objN);
  end;
end;

constructor TOutputLayer.Create(ParenTNeuralNetwork: TNeuralNetwork;
  LayerElement: TDOMElement);
begin
  inherited Create(ParenTNeuralNetwork, LayerElement, nkOutput);
end;

function TOutputLayer.Add(N: TOutputNeuron): Integer;
begin
  Result := inherited Add(N);
end;

function TOutputLayer.Remove(N: TOutputNeuron): Integer;
begin
  Result := inherited Remove(N);
end;

function TOutputLayer.GetItem(Index: Integer): TOutputNeuron;
begin
  Result := inherited GetItem(Index) as TOutputNeuron;
end;

{ THiddenLayer }

constructor THiddenLayer.Create(ParentNework: TNeuralNetwork; ANeuronCount: Integer);
var
  N: Integer;
  objN: THiddenNeuron;
begin
  inherited Create(ParentNework);
  FKind := nkHidden;
  objN := THiddenBias.Create;
  Add(objN);
  for N := 1 to ANeuronCount do
  begin
    objN := THiddenNeuron.Create;
    Add(objN);
  end;
end;

constructor THiddenLayer.Create(ParenTNeuralNetwork: TNeuralNetwork;
  LayerElement: TDOMElement);
begin
  inherited Create(ParenTNeuralNetwork, LayerElement, nkHidden);
end;

function THiddenLayer.Add(N: THiddenNeuron): Integer;
begin
  Result := inherited Add(N);
end;

function THiddenLayer.Remove(N: THiddenNeuron): Integer;
begin
  Result := inherited Remove(N);
end;

function THiddenLayer.GetItem(Index: Integer): THiddenNeuron;
begin
  Result := inherited GetItem(Index) as THiddenNeuron;
end;

{ TInputLayer }

constructor TInputLayer.Create(ParentNetwork: TNeuralNetwork; ANeuronCount: Integer);
var
  N: Integer;
  objN: TInputNeuron;
begin
  inherited Create(ParentNetwork);
  FKind := nkInput;
  objN := TInputBias.Create;
  Add(objN);
  for N := 1 to ANeuronCount do
  begin
    objN := TInputNeuron.Create;
    Add(objN);
  end;
end;

constructor TInputLayer.Create(ParentNetwork: TNeuralNetwork;
  LayerElement: TDOMElement);
begin
  inherited Create(ParentNetwork, LayerElement, nkInput);
end;

function TInputLayer.Add(N: TInputNeuron): Integer;
begin
  Result := inherited Add(N);
end;

function TInputLayer.Remove(N: TInputNeuron): Integer;
begin
  Result := inherited Remove(N);
end;

function TInputLayer.GetItem(Index: Integer): TInputNeuron;
begin
  Result := inherited GetItem(Index) as TInputNeuron;
end;

{ TNeuralNetwork }

function TNeuralNetwork.GetLayer(Index: Integer): TNeuronLayer;
begin
  Result := inherited Items[Index] as TNeuronLayer;
end;

constructor TNeuralNetwork.Create(LayerSizes: array of const);
var
  S, Size: Integer;
begin
  inherited Create;
  FLearningRate := DefaultLearningRate;
  for S:=0 to Length(LayerSizes) - 1 do
  begin
    Size := LayerSizes[S].VInteger;
    if S = 0 then
      AddInputLayer(Size)
    else if S = Length(LayerSizes) - 1 then
      AddOutputLayer(Size)
    else
      AddHiddenLayer(Size);
  end;
  FDrawFlag := True;
end;

constructor TNeuralNetwork.Create(FilePath: String);
var
  Doc: TXMLDocument;
  NetworkElement: TDOMElement;
  sLearningRate: String;
  eLearningRate: Extended;
  sSigmoidSlope: String;
  eSigmoidSlope: Extended;
  LayerElementList: TDOMElementList;
  LayerElement: TDOMElement;
  sLayerKind: String;
  iLayerKind: Integer;
  LayerKind: TNeuronKind;
  L: Integer;
  objL: TNeuronLayer;
begin
  // Create an empty network
  Create([]);

  try
    // Read the XML document into an object
    ReadXMLFile(Doc, FilePath);
    // Get the root (network) element
    NetworkElement := Doc.DocumentElement;
    if NetworkElement = nil then
      raise ENetworkError.Create('Error loading the network: Missing network ' +
        'node');
    // Read learning rate
    if NetworkElement.hasAttribute(txtLearningRate) then
    begin
      sLearningRate := NetworkElement.GetAttribute(txtLearningRate);
      eLearningRate := StrToFloat(sLearningRate);
      FLearningRate := eLearningRate;
    end;
    if NetworkElement.hasAttribute(txtSigmoidSlope) then
    begin
      sSigmoidSlope := NetworkElement.GetAttribute(txtSigmoidSlope);
      eSigmoidSlope := StrToFloat(sSigmoidSlope);
      SigmoidSlope := eSigmoidSlope;
    end;
    // Get layers
    LayerElementList := TDOMElementList.Create(NetworkElement, 'Layer');
    for L := 0 to LayerElementList.Count - 1 do
    begin
      LayerElement := LayerElementList[L] as TDOMElement;
      if LayerElement.NodeType = ELEMENT_NODE then
      begin
        sLayerKind := LayerElement['Kind'];
        iLayerKind := StrToInt(sLayerKind);
        LayerKind := TNeuronKind(iLayerKind);
        if LayerKind = nkInput then
        begin
          objL := TInputLayer.Create(Self, LayerElement);
          AddInputLayer(objL as TInputLayer);
        end
        else if LayerKind = nkHidden then
        begin
          objL := THiddenLayer.Create(Self, LayerElement);
          AddHiddenLayer(objL as THiddenLayer);
        end
        else if LayerKind = nkOutput then
        begin
          objL := TOutputLayer.Create(Self, LayerElement);
          AddOutputLayer(objL as TOutputLayer);
        end;
      end;
    end;
  finally
    LayerElementList.Free;
    NetworkElement.Free;
    Doc.Free;
  end;

end;

function TNeuralNetwork.GetNeuron(LayerIndex, NeuronIndex: Integer): TNeuron;
begin
  Result := Layers[LayerIndex].Items[NeuronIndex] as TNeuron;
end;


(*
function TNeuralNetwork.AddNeuron(ItsLayer: Integer{; X: Integer; Y: Integer}): TNeuron;
var
  objLayer: TNeuronLayer;
  //INeu: Integer;
begin
  if ItsLayer > Count - 1 then
  begin
    objLayer := TNeuronLayer.Create(Self);
    Add(objLayer);
  end
  else
    objLayer := Self[ItsLayer];

  Result := TNeuron.Create{(Self)};
  //Result.Parent := Self;
  
  {INeu :=} objLayer.Add(Result);
  
  {if X = 0 then
    Result.Left := Result.Width div 2 + Result.Width * 2 * INeu
  else
    Result.Left := X;
  if Y = 0 then
    Result.Top := Result.Height div 2 + Result.Height * 2 * ItsLayer
  else
    Result.Top := Y;}

end;
*)

{procedure TNeuralNetwork.Paint;
var
  V: Integer;
  PuvodniKresleni: Boolean;
begin
  inherited Paint;
  PuvodniKresleni := FDraw;
  FDraw := True;
  for V := 0 to LayerList.Count - 1 do
  begin
    Layer[V].PaintSynapses();
  end;
  FDraw := PuvodniKresleni;
end;}

procedure TNeuralNetwork.Evaluate(DesiredOutput: array of Extended);
var
  objOutputLayer: TOutputLayer;
  objLayer, objNextLayer: TNeuronLayer;
  Sum, ErrorBase: Extended;
  N, I, J, L: Integer;
  objN, objI, objJ: TNeuron;
  SyList: TSynapseList;
  {$IfNDef NEURON_DYNAMIC_VALUE}
  objS: TSynapse;
  {$EndIf}
  Value: Extended;
  //Derivative: Extended;
begin
  objOutputLayer := OutputLayer;
  //objOutputLayer.Evaluate(DesiredOutput);


  {$IfNDef NEURON_DYNAMIC_VALUE}
  // Calculate values:
  for L := 0 to LayerCount - 1 do
  begin
    objLayer := Layers[L];
    for I := 0 to objLayer.Count - 1 do
    begin
      objI := objLayer[I];
      SyList := objI.BackwardSynapseList;
      Sum := 0;
      for J := 0 to SyList.Count - 1 do
      begin
        objS := SyList[J];
        objJ := objS.InputNeuron;
        if objS.InputNeuron = nil then
          Sum += objS.Weight
        else
          Sum += objJ.Value * objS.Weight;
      end;
      // Normalization to <-1;1> happens on assignment
      if Assigned(objI.ActivationFunction.Func) then
        objI.Value := objI.ActivationFunction.Func(Sum)
      else
        objI.Value := Sum;
    end;
  end;
  {$EndIf}

  // Calculate errors:
  for N := 0 to objOutputLayer.Count - 1 do
  begin
    objN := objOutputLayer[N];
    Value := objN.Value;
    ErrorBase := DesiredOutput[N] - Value;
    //Derivative := objN.CalculateDerivative;
    objN.Error := ErrorBase {* Derivative};
  end;

  for L := LayerCount - 2 downto 0 do
  begin
    objLayer := Layers[L];
    objNextLayer := Layers[L + 1];
    for I := 0 to objLayer.Count - 1 do
    begin
      objI := objLayer[I];
      Sum:=0.0;
      for J := 0 to objNextLayer.Count - 1 do
      begin
        objJ := objNextLayer[J];
        if objJ.Kind <> nkHiddenBias then
        begin
          Sum += objJ.Error * objJ.BackwardSynapses[I].Weight;
        end;
      end;
      objI.Error := objI.CalculateDerivative * Sum;
    end;
  end;

  // Calculate weights:
  for L := 1 to LayerCount - 1 do
  begin
    objLayer := Layers[L];
    for I := 0 to objLayer.Count - 1 do
    begin
      objI := objLayer[I];
      SyList := objI.BackwardSynapseList;
      for J := 0 to SyList.Count - 1 do
      begin
        objJ := SyList[J].InputNeuron;
        {SyList[J].Weight := SyList[J].Weight +
          FLearningRate * objI.Error * objJ.Value;}
        if objJ <> nil then
          SyList[J].AdjustedWeight := SyList[J].Weight +
            FLearningRate * objI.Error * objJ.Value;
      end;
    end;
  end;

  // Adjust weights
  ApplyAdjustedWeights;

  //if DrawFlag then Repaint;
end;

procedure TNeuralNetwork.Backpropagate(Errors: array of Extended);
var
  objOutputLayer: TOutputLayer;
  objLayer, objNextLayer: TNeuronLayer;
  Sum, ErrorBase: Extended;
  N, I, J, L: Integer;
  objN, objI, objJ: TNeuron;
  SyList: TSynapseList;
  {$IfNDef NEURON_DYNAMIC_VALUE}
  objS: TSynapse;
  {$EndIf}
  //Value: Extended;
  Derivative: Extended;
begin
  objOutputLayer := OutputLayer;
  //objOutputLayer.Evaluate(DesiredOutput);


  {$IfNDef NEURON_DYNAMIC_VALUE}
  // Calculate values:
  for L := 0 to LayerCount - 1 do
  begin
    objLayer := Layers[L];
    for I := 0 to objLayer.Count - 1 do
    begin
      objI := objLayer[I];
      SyList := objI.BackwardSynapseList;
      Sum := 0;
      for J := 0 to SyList.Count - 1 do
      begin
        objS := SyList[J];
        objJ := objS.InputNeuron;
        if objS.InputNeuron = nil then
          Sum += objS.Weight
        else
          Sum += objJ.Value * objS.Weight;
      end;
      // Normalization to <-1;1> happens on assignment
      if Assigned(objI.ActivationFunction.Func) then
        objI.Value := objI.ActivationFunction.Func(Sum)
      else
        objI.Value := Sum;
    end;
  end;
  {$EndIf}

  // Calculate errors:
  for N := 0 to objOutputLayer.Count - 1 do
  begin
    objN := objOutputLayer[N];
    //Value := objN.Value;
    ErrorBase := Errors[N];
    Derivative := objN.CalculateDerivative;
    objN.Error := ErrorBase * Derivative;
  end;

  for L := LayerCount - 2 downto 0 do
  begin
    objLayer := Layers[L];
    objNextLayer := Layers[L + 1];
    for I := 0 to objLayer.Count - 1 do
    begin
      objI := objLayer[I];
      Sum:=0.0;
      for J := 0 to objNextLayer.Count - 1 do
      begin
        objJ := objNextLayer[J];
        if objJ.Kind <> nkHiddenBias then
          Sum += objJ.Error * objJ.BackwardSynapses[I].Weight;
      end;
      objI.Error := objI.CalculateDerivative * Sum;
    end;
  end;

  // Calculate weights:
  for L := 1 to LayerCount - 1 do
  begin
    objLayer := Layers[L];
    for I := 0 to objLayer.Count - 1 do
    begin
      objI := objLayer[I];
      SyList := objI.BackwardSynapseList;
      for J := 0 to SyList.Count - 1 do
      begin
        objJ := SyList[J].InputNeuron;
        {SyList[J].Weight := SyList[J].Weight +
          FLearningRate * objI.Error * objJ.Value;}
        if objJ <> nil then
          SyList[J].AdjustedWeight := SyList[J].Weight +
            FLearningRate * objI.Error * objJ.Value;
      end;
    end;
  end;

  // Adjust weights
  ApplyAdjustedWeights;

end;

function TNeuralNetwork.GetLayerCount: Integer;
begin
  Result := Count;
end;

function TNeuralNetwork.GetInputLayer: TInputLayer;
begin
  Result := Layers[0] as TInputLayer;
end;

function TNeuralNetwork.GetHiddenLayer(Index: Integer): THiddenLayer;
begin
  Result := Layers[Index + 1] as THiddenLayer;
end;

function TNeuralNetwork.GetHiddenLayerCount: Integer;
begin
  Result := GetLayerCount - 2;
end;

function TNeuralNetwork.GetOutputLayer: TOutputLayer;
begin
  Result := Layers[LayerCount - 1] as TOutputLayer;
end;

function TNeuralNetwork.GetAvgError: Extended;
var
  objOutputLayer: TOutputLayer;
begin
  objOutputLayer := OutputLayer;
  if objOutputLayer <> nil then
    Result := objOutputLayer.GetAvgError
  else
    Result := 0;
end;

procedure TNeuralNetwork.SetLearningRate(AValue: Extended);
begin
  if FLearningRate=AValue then Exit;
  FLearningRate:=AValue;
end;

constructor TNeuralNetwork.Create;
begin
  Create([]);
end;

function TNeuralNetwork.AddInputLayer(L: TInputLayer): Integer;
begin
  Result := inherited Add(L);
  L.LayerIndex := Result;
end;

function TNeuralNetwork.AddInputLayer(NeuronCount: Integer): TInputLayer;
begin
  Result := TInputLayer.Create(Self, NeuronCount);
  AddInputLayer(Result);
end;

function TNeuralNetwork.AddHiddenLayer(L: THiddenLayer): Integer;
var
  PreviousLayer: TNeuronLayer;
begin
  Result := inherited Add(L);
  L.LayerIndex := Result;
  if not L.Connected then
  begin
    PreviousLayer := Layers[Result - 1];
    ConnectLayers(PreviousLayer, L);
  end;
end;

function TNeuralNetwork.AddHiddenLayer(NeuronCount: Integer): THiddenLayer;
begin
  Result := THiddenLayer.Create(Self, NeuronCount);
  AddHiddenLayer(Result);
end;

function TNeuralNetwork.AddOutputLayer(L: TOutputLayer): Integer;
var
  PreviousLayer: TNeuronLayer;
begin
  Result := inherited Add(L);
  L.LayerIndex := Result;
  if not L.Connected then
  begin
    PreviousLayer := Layers[Result - 1];
    ConnectLayers(PreviousLayer, L);
  end;
end;

function TNeuralNetwork.AddOutputLayer(NeuronCount: Integer): TOutputLayer;
begin
  Result := TOutputLayer.Create(Self, NeuronCount);
  AddOutputLayer(Result);
end;

procedure TNeuralNetwork.ConnectLayers(InputLayer, OutputLayer: TNeuronLayer);
var
  I, J: Integer;
  InputNeuron, OutputNeuron: TNeuron;
begin
  for I:=0 to OutputLayer.Count - 1 do
  begin
    OutputNeuron := OutputLayer[I];
    if OutputNeuron.Kind <> nkHiddenBias then
    begin
      for J:=0 to InputLayer.Count - 1 do
      begin
        InputNeuron := InputLayer[J];
        OutputLayer.AddBackwardSynapse(InputNeuron, OutputNeuron);
        InputLayer.AddForwardSynapse(InputNeuron, OutputNeuron);
      end;
    end;
  end;
end;

procedure TNeuralNetwork.ApplyAdjustedWeights;
var
  L: Integer;
  objL: TNeuronLayer;
begin
  for L := 0 to HiddenLayerCount - 1 do
  begin
    objL := HiddenLayers[L];
    objL.ApplyAdjustedWeights;
  end;
  objL := OutputLayer;
  objL.ApplyAdjustedWeights;
end;

procedure TNeuralNetwork.SaveYourself(Path: String);
var
  Doc: TXMLDocument;
  L: Integer;
  objL: TNeuronLayer;
  NetworkElement: TDOMElement;
  sLearningRate: String;
  sSigmoidSlope: String;
begin
  try
    Doc := TXMLDocument.Create;
    NetworkElement := Doc.CreateElement('Network');
    sLearningRate := FloatToStr(FLearningRate);
    NetworkElement.SetAttribute(txtLearningRate, sLearningRate);
    sSigmoidSlope := FloatToStr(SigmoidSlope);
    NetworkElement.SetAttribute(txtSigmoidSlope, sSigmoidSlope);
    for L := 0 to LayerCount - 1 do
    begin
      objL := Layers[L];
      objL.SaveYourself(Doc, NetworkElement);
    end;
    Doc.AppendChild(NetworkElement);
    WriteXMLFile(Doc, Path);
  finally
    Doc.Free;
  end;
end;

{ Make a “blank table” out of the network
- chaotize connection weights - the network initializes and
“forgets” what it has learned so far. }
procedure TNeuralNetwork.FacTabulamRasam;
var
  La, Ce, Sy: Integer; // counters
  objLa: TNeuronLayer;
  objCe: TNeuron;
  objSy: TSynapse;
  SyLi: TSynapseList;
begin
  //Randomize;
  for La := 1 {skip the input Layers - there the weights mean inputs}
  to LayerCount - 1 do
  begin
    objLa := Items[La];
    for Ce := 0 to objLa.Count - 1 do
    begin
      objCe := objLa[Ce];
      SyLi := objCe.BackwardSynapseList;
      for Sy := 0 to SyLi.Count - 1 do
      begin
        objSy := SyLi[Sy];
        if objSy.InputNeuron <> nil {to be sure} then
          objSy.Weight := Random * 2 - 1 // -1..+1
      end;
    end;
  end;
end;

{ TNeuronLayer }

function TNeuronLayer.GetItem(Index: Integer): TNeuron;
begin
  Result := inherited Items[Index];
end;

function TNeuronLayer.GetMaxWeight: Extended;
var
  N: Integer;
  W: Extended;
begin
  Result := Self.Items[0].GetMaxWeight;
  for N := 0 to Self.Count - 1 do
  begin
    W:=Self.Items[N].GetMaxWeight;
    if W > Result then
      Result := W;
  end;
end;

procedure TNeuronLayer.ApplyAdjustedWeights;
var
  N: Integer;
  objN: TNeuron;
begin
  for N := 0 to Count - 1 do
  begin
    objN := Items[N];
    objN.ApplyAdjustedWeights;
  end;
end;

function TNeuronLayer.GetAvgError: Extended;
var
  N: Integer;
  objN: TNeuron;
  Sum: Extended;
begin
  Sum := 0;
  for N := 0 to Count - 1 do
  begin
    objN := Items[N];
    Sum += abs(objN.Error);
  end;
  Result := Sum / Count;
end;

function TNeuronLayer.GetNeuronCount: Integer;
begin
  Result := Count;
end;

procedure TNeuronLayer.SaveYourself(Doc: TXMLDocument; NetworkElement: TDOMElement);
var
  LayerElement: TDOMElement;
  N: Integer;
  objN: TNeuron;
begin
  LayerElement := Doc.CreateElement('Layer');
  LayerElement['Kind'] := IntToStr(Integer(FKind));
  LayerElement['LayerIndex'] := FloatToStr(FLayerIndex);
  for N := 0 to Count - 1 do
  begin
    objN := Items[N];
    objN.SaveYourself(Doc, LayerElement);
  end;
  NetworkElement.AppendChild(LayerElement);
end;

procedure TNeuronLayer.SetLayerIndex(AValue: Integer);
var
  N: Integer;
  objN: TNeuron;
begin
  if FLayerIndex = AValue then Exit;
  FLayerIndex := AValue;
  for N := 0 to Count - 1 do
  begin
    objN := Items[N];
    objN.LayerIndex := AValue;
  end;
end;

constructor TNeuronLayer.Create(ParentNetwork: TNeuralNetwork);
begin
  inherited Create(True);
  FNetwork := ParentNetwork;
end;

constructor TNeuronLayer.Create(ParentNetwork: TNeuralNetwork; LayerElement: TDOMElement;
  LayerKind: TNeuronKind);
var
  N, S: Integer;
  objN: TNeuron;
  NeuronElementList: TDOMElementList;
  NeuronElement: TDOMElement;
  NeuronKind: TNeuronKind;
  objS: TSynapse;
  SynapseElementList: TDOMElementList;
  SynapseElement: TDOMElement;
  LI, NI: Integer;
  Weight: Extended;
  FS: TFormatSettings;
begin
  // Create an empty layer owning its layers
  inherited Create(True);
  // Assign the parent network reference
  FNetwork := ParentNetwork;
  // Set the Kind read-only property
  FKind := LayerKind;
  try
    // Get the child nodes of LayerNode of type element and of the name 'Neuron'
    NeuronElementList := TDOMElementList.Create(LayerElement, 'Neuron');
    // Go through the element list
    for N := 0 to NeuronElementList.Count - 1 do
    begin
      // Surprisingly, we have to convert the element from TDOMNode
      NeuronElement := NeuronElementList[N] as TDOMElement;
      // Let's create the neuron and add it; synapse stuff later
      // Read the neuron kind from the element
      NeuronKind := TNeuronKind(StrToInt(NeuronElement['Kind']));
      objN := CreateNeuronOfKind(NeuronKind);
      Add(objN);
      { Synapse stuff goes here.
        We can't handle it inside the TNeuron or TSynapse class because they
        are aware neither of the enclosing layer nor of the network, so they
        can't be aware of the other neurons as objects unless the synapse
        neurons are set from the outside.
        The neurons can't be aware of the network class because the class is
        declared in another unit and a unit cross-reference would happen; and
        it's not possible to forward declare a class that's declared in another
        unit. }
      // Format settings for float reading
      FS := DefaultFormatSettings;
      FS.DecimalSeparator := '.';
      { Get the child nodes of NeuronElement of type element and of the name
        'Synapse' }
      SynapseElementList := TDOMElementList.Create(NeuronElement, 'Synapse');
      if NeuronKind = nkInput then
      begin
        if SynapseElementList.Count = 1 then
        begin
          SynapseElement := SynapseElementList[0] as TDOMElement;
          Weight := StrToFloat(SynapseElement['Weight'], FS);
          (objN as TInputNeuron).Input := Weight;
        end
        else
          raise ENetworkError.Create('Error loading the network: An input ' +
            'neuron must have exactly one input synapse.');
      end
      else if NeuronKind in [nkInputBias, nkHiddenBias] then
      begin
        // Nothing to add or change for a bias node.
      end
      else
      begin
        // Go through the synapse list
        for S := 0 to SynapseElementList.Count - 1 do
        begin
          // Surprisingly, we need to convert the SynapseElement from TDOMNode
          SynapseElement := SynapseElementList[S] as TDOMElement;
          // Get the layer and neuron index
          LI := StrToInt(SynapseElement['LayerIndex']);
          NI := StrToInt(SynapseElement['NeuronIndex']);
          // Create and add the synapse
          objS := objN.AddBackwardSynapse(Network.Neurons[LI, NI]);
          // Get the synapse weight
          objS.Weight:=StrToFloat(SynapseElement['Weight'], FS);
        end;
      end;
    end;
    FConnected := True;
  finally
    NeuronElementList.Free;
  end;
end;

function TNeuronLayer.Add(N: TNeuron): Integer;
begin
  //N.OnChange := @OnSynapseChange;
  Result := inherited Add(N);
  N.NeuronIndex := Result;
  N.LayerIndex := LayerIndex;
end;

function TNeuronLayer.Remove(N: TNeuron): Integer;
begin
  Result := inherited Remove(N);
end;

function TNeuronLayer.AddBackwardSynapse(Input, Output: TNeuron): TSynapse;
var
  Sy: TSynapse;
begin
  Sy := Output.AddBackwardSynapse(Input);
  //DrawSynapse(Output, Sy);
  Result := Sy;
end;

function TNeuronLayer.AddForwardSynapse(Input, Output: TNeuron): TSynapse;
var
  Sy: TSynapse;
begin
  Sy := Input.AddForwardSynapse(Output);
  //DrawSynapse(Output, Sy);
  Result := Sy;
end;

function TNeuronLayer.GetDrawFlag: Boolean;
begin
  if FNetwork <> nil then
  begin
    if FNetwork is TNeuralNetwork then
      Result := FNetwork.DrawFlag
    else // FNetwork is not of type TNeuralNetwork
      Result := False;
  end
  else // FNetwork is nil
    Result := False;
end;

procedure TOutputLayer.Evaluate(EstimatedOutput: array of Extended);
var
  N: Integer;
  Err: Extended;
begin
  if Length(EstimatedOutput) <> Count then
  begin
    raise ENetworkError.Create(
      'Given output array size isn''t equal to output neuron count.'
    );
  end
  else
  begin
    for N := 0 to Count - 1 do
    begin
      Err := EstimatedOutput[N] - Items[N].Value;
      Items[N].Evaluate(Err);
    end;
  end;
end;

end.

