unit COCRUtils;

{$mode objfpc}

interface

uses
  Classes, SysUtils, {Network, Neuron,} BetaNetwork, LCLType, Graphics{, Math},
  Forms, LazFileUtils;

const
  // Teaching result kinds
  trkBeforeTeaching = 0;
  trkAfterTeaching = 1;

type
  TTeachingResult = array[trkBeforeTeaching..trkAfterTeaching] of UnicodeChar;
  TEpochResult = array[trkBeforeTeaching..trkAfterTeaching] of UnicodeString;

function OurPath(FileName: String): String;
function NetworkOutputToCharCode(Network: TNeuralNetwork): Word;
function CharCodeToTargetNetworkOutput(Code: Word): TNeuralFloatArray;
function Gray(Value: Extended): TColor;
function RandomizeString(InputString: UnicodeString): UnicodeString;
function GetCOCRFormatSettings: TFormatSettings;
operator+(EpochResult: TEpochResult; TeachingResult: TTeachingResult):
    TEpochResult;
operator=(EpochResult: TEpochResult; S: UnicodeString): Boolean;
operator:=(S: UnicodeString): TEpochResult;
operator:=(ER: TEpochResult): UnicodeString;

const
  txtLearningRate = 'LearningRate';
  BitCount = 16;
  CharCount = 1 shl BitCount;
  HighestChar = CharCount - 1;


implementation

function OurPath(FileName: String): String;
var
  RealExecutable: String;
  AppDir: String;
begin
  RealExecutable := ReadAllLinks(Application.ExeName, False);
  AppDir := ExtractFilePath(RealExecutable);
  Result := AppDir + FileName;
end;

function NetworkOutputToCharCode(Network: TNeuralNetwork): Word;
var
  N: Integer;
  objOutputLayer: {TOutputLayer} TNeuronLayer;
  HighestN: Integer;
  CharCode: Word;
  Outputs: TNeuralFloatArray;
  Output: Single;
begin
  CharCode := 0;
  if Assigned(Network) then
  begin
    objOutputLayer := Network.OutputLayer;
    //HighestN := objOutputLayer.Count - 1;
    Outputs := objOutputLayer.get_outputs();
    HighestN := High(Outputs);
    for N := 0 to HighestN do
    begin
      Output := Outputs[N];
      {CharCode := CharCode or ((1 shl N) * MathRound((objN.Value + 1) / 2));}
      CharCode := CharCode or ((1 shl N) * MathRound(Output));
      {if Output >= Network.OutputThreshold then
        CharCode := CharCode or (1 shl N);}
    end;
  end;

  Result := CharCode;
end;

function CharCodeToTargetNetworkOutput(Code: Word): TNeuralFloatArray;
var
  B: Integer;
begin
  Result := TNeuralFloatArray.Create;
  SetLength(Result, BitCount);
  for B := Low(Result) to High(Result) do
  begin
    if (Code and (1 shl B)) > 0 then
      Result[B] := 1 // 1-bit
    else
      // Result[B] := -1; // 0-bit
      Result[B] := 0; // 0-bit
  end;
end;

function Gray(Value: Extended): TColor;
var
  Component: Byte;
begin
  if Value < -1 then value := -1;
  if Value > 1 then Value := 1;
  Component := round((Value + 1) / 2 * 255);
  Result := RGBToColor(Component, Component, Component);
end;

function RandomizeString(InputString: UnicodeString): UnicodeString;
var
  Len: Integer;
  ChoiceCharNumber: Integer;
  ChoiceChar: UnicodeChar;
begin
  Result := '';
  Len := Length(InputString);
  while Len > 0 do
  begin
    ChoiceCharNumber := Random(Len) + 1;
    ChoiceChar := InputString[ChoiceCharNumber];
    Delete(InputString, ChoiceCharNumber, 1);
    Result += ChoiceChar;
    Len := Length(InputString);
  end;
end;

function GetCOCRFormatSettings: TFormatSettings;
begin
  Result := DefaultFormatSettings;
  Result.DecimalSeparator:='.';
end;

operator+(EpochResult: TEpochResult; TeachingResult: TTeachingResult
  ): TEpochResult;
begin
  Result[trkBeforeTeaching] :=
    EpochResult[trkBeforeTeaching] + TeachingResult[trkBeforeTeaching];
  Result[trkAfterTeaching] :=
    EpochResult[trkAfterTeaching] + TeachingResult[trkAfterTeaching];
end;

operator=(EpochResult: TEpochResult; S: UnicodeString): Boolean;
begin
  Result :=
    (EpochResult[trkBeforeTeaching] = S) and
    (EpochResult[trkAfterTeaching] = S);
end;

operator:=(S: UnicodeString): TEpochResult;
begin
  Result[trkBeforeTeaching] := S;
  Result[trkAfterTeaching] := S;
end;

operator:=(ER: TEpochResult): UnicodeString;
begin
  Result := ER[trkBeforeTeaching] + UTF8Decode('→') + ER[trkAfterTeaching];
end;

end.

