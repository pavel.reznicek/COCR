unit NetworkTestForm;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Spin, StdCtrls, ComCtrls, fgl, Neuron, Network, COCRUtils;

const
  nvNeuronDiameter = 32;
  nvNeuronRadius = nvNeuronDiameter div 2;
  nvNeuronMargin = 15;
  nvNeuronDistance = nvNeuronDiameter + 2 * nvNeuronMargin;
  nvSynapseRowWidth = 100;
  nvLayerMargin = 15;
  nvLayerDistance = nvNeuronDiameter + nvLayerMargin + nvSynapseRowWidth +
    nvLayerMargin;

const
  Inputs: array[0..3, 0..1] of Extended = ((-1, -1), (-1, 1), (1, -1), (1, 1));
  Outputs: array[0..3, 0..0] of Extended = ((-1), (1), (1), (-1));

type

  { TVisualSynapse }

  TVisualSynapse = class(TFloatSpinEdit)
  private
    FSynapse: TSynapse;
    procedure SynapseChange(Sender: TObject);
  public
    constructor Create(TheOwner: TComponent; BackendSynapse: TSynapse);
      overload;
    procedure Change; override;
    property Synapse: TSynapse read FSynapse;
  end;

  { TSynapseVisualiser }
  TVisualSynapseList = specialize TFPGObjectList<TVisualSynapse>;

  { TVisualNeuron }
  TVisualNeuron = class(TShape)
  private
    FNeuron: TNeuron;
    FSelected: Boolean;
    FSynapseList: TVisualSynapseList;
    function GetVisualSynapse(Index: Integer): TVisualSynapse;
    procedure SetSelected(AValue: Boolean);
    procedure SynapseChange(Sender: TObject);
    procedure NeuronChange(Sender: TObject);
    procedure ShowValue;
  public
    constructor Create(TheOwner: TComponent; BackendNeuron: TNeuron);
      overload;
    procedure Click; override;
    property VisualSynapses[Index: Integer]: TVisualSynapse
      read GetVisualSynapse;
  published
    property Neuron: TNeuron read FNeuron;
    property Selected: Boolean read FSelected write SetSelected;
  end;

  { TVisualNeuronList }
  TVisualNeuronList = specialize TFPGObjectList<TVisualNeuron>;

  { TVisualLayer }

  TVisualLayer = class(TVisualNeuronList)
  private
    FLayer: TLayer;
    FNeuronList: TVisualNeuronList;
    function GetVisualNeuron(Index: Integer): TVisualNeuron;
    procedure VisualNeuronClick(Sender: TObject);
  public
    constructor Create(TheOwner: TComponent; BackendLayer: TLayer); overload;
    property VisualNeurons[Index: Integer]: TVisualNeuron read GetVisualNeuron;
    property Layer: TLayer read FLayer;
  end;

  { TVisualLayerList }
  TVisualLayerList = specialize TFPGObjectList<TVisualLayer>;

  { TNetworkVisualiser }

  TNetworkVisualiser = class(TScrollBox)
  private
    FNet: TNetwork;
    FLayerList: TVisualLayerList;
  public
    constructor Create(AOwner: TControl; ANet: TNetwork); overload;
    destructor Destroy; override;
    procedure Layout;
    procedure RemoveChildern;
  published
    property Net: TNetwork read FNet;
  end;

  { TFrmNetworkTest }

  TFrmNetworkTest = class(TForm)
    btEpoch: TButton;
    btInput: TButton;
    btEvaluate: TButton;
    gbControls: TGroupBox;
    lbPatterns: TListBox;
    mmLog: TMemo;
    panTrainig: TPanel;
    pbProgress: TProgressBar;
    tbTrain: TToggleBox;
    procedure btEpochClick(Sender: TObject);
    procedure btEvaluateClick(Sender: TObject);
    procedure btInputClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure tbTrainChange(Sender: TObject);
  private
    { private declarations }
    procedure Epoch;
    procedure FeedInput;
    procedure Evaluate;
  public
    { public declarations }
    TestNet: TNetwork;
    TestVis: TNetworkVisualiser;
  end;

var
  FrmNetworkTest: TFrmNetworkTest;

implementation

{$R *.lfm}

{ TVisualLayer }

function TVisualLayer.GetVisualNeuron(Index: Integer): TVisualNeuron;
begin
  Result := FNeuronList.Items[Index];
end;

procedure TVisualLayer.VisualNeuronClick(Sender: TObject);
var
  SenderNeuron: TVisualNeuron;
  N: Integer;
  objVN: TVisualNeuron;
begin
  if Sender is TVisualNeuron then
    SenderNeuron := Sender as TVisualNeuron;
  for N := 0 to FNeuronList.Count - 1 do
  begin
    objVN := FNeuronList.Items[N];
    if objVN <> SenderNeuron then
      objVN.Selected := False;
  end;
end;

constructor TVisualLayer.Create(TheOwner: TComponent; BackendLayer: TLayer);
var
  N: Integer;
  objN: TNeuron;
  objVN: TVisualNeuron;
begin
  inherited Create(False);
  FLayer := BackendLayer;
  FNeuronList := TVisualNeuronList.Create(False); { the owner owns the visual
    neuron objects }
  for N := 0 to FLayer.Count - 1 do
  begin
    objN := FLayer.Neurons[N];
    objVN := TVisualNeuron.Create(TheOwner, objN);
    FNeuronList.Add(objVN);
    objVN.Left := nvLayerMargin + nvSynapseRowWidth + nvLayerMargin +
      objN.LayerIndex * nvLayerDistance;
    objVN.Top := nvNeuronMargin + objN.NeuronIndex * nvNeuronDistance;
    objVN.Width := nvNeuronDiameter;
    objVN.Height := nvNeuronDiameter;
    objVN.OnClick := @VisualNeuronClick;
  end;
end;

{ TVisualNeuron }

function TVisualNeuron.GetVisualSynapse(Index: Integer): TVisualSynapse;
begin
  Result := FSynapseList[Index];
end;

procedure TVisualNeuron.SetSelected(AValue: Boolean);
var
  S: Integer;
  objVS: TVisualSynapse;
begin
  if FSelected=AValue then Exit;
  FSelected:=AValue;
  if FSelected then
    Pen.Width:=2
  else
    Pen.Width:=1;
  if (FNeuron is THiddenNeuron) or
    (FNeuron is TOutputNeuron) then
  begin
    for S := 0 to FSynapseList.Count - 1 do
    begin
      objVS := FSynapseList.Items[S];
      objVS.Visible := Selected;
    end;
  end;
end;

procedure TVisualNeuron.SynapseChange(Sender: TObject);
begin
  ShowValue;
end;

procedure TVisualNeuron.NeuronChange(Sender: TObject);
begin
  ShowValue;
end;

procedure TVisualNeuron.ShowValue;
var
  V: Extended;
begin
  V := FNeuron.Value;
  Hint := FloatToStr(V);
  Brush.Color:=Gray(V);
end;

procedure TVisualNeuron.Click;
begin
  inherited Click;
  Selected := True;
end;

constructor TVisualNeuron.Create(TheOwner: TComponent;
  BackendNeuron: TNeuron);
var
  S: Integer;
  objS: TSynapse;
  objVS: TVisualSynapse;
  SynapseVerticalPosition: Integer; { what horizontal row will the synapse
    be displayed in }
begin
  inherited Create(TheOwner);
  Shape := stCircle;
  ShowHint:=True;
  if TheOwner is TWinControl then
    Parent := TheOwner as TWinControl;
  FNeuron := BackendNeuron;

  { Backend Synapses can't have double-assigned events; therefore we have to
  watch for change events on the TVisualSynapse objects }

  FNeuron.OnChange := @NeuronChange;

  // Create the visual synapses and place them on the owner
  FSynapseList := TVisualSynapseList.Create(False);
  for S := 0 to FNeuron.BackwardSynapseCount - 1 do
  begin
    objS := FNeuron.BackwardSynapses[S];
    objVS := TVisualSynapse.Create(TheOwner, objS);
    objVS.Left := nvLayerMargin + FNeuron.LayerIndex * nvLayerDistance;
    if FNeuron is TInputNeuron then
      SynapseVerticalPosition := FNeuron.NeuronIndex
    else
      SynapseVerticalPosition := S;
    objVS.Top := nvNeuronMargin + SynapseVerticalPosition * nvNeuronDistance +
      nvNeuronRadius - (objVS.Height div 2) - 2 { mysterious 2px lag };
    objVS.Width := nvSynapseRowWidth;
    objVS.Visible := (FNeuron is TInputNeuron);
    if (FNeuron is TInputBias) or
      (FNeuron is THiddenBias) then
      objVS.ReadOnly := True;
    objVS.OnChange := @SynapseChange;
    FSynapseList.Add(objVS);
  end;

  // Set hint and background colour
  ShowValue;
end;

{ TVisualSynapse }

procedure TVisualSynapse.SynapseChange(Sender: TObject);
begin
  Value := FSynapse.Weight;
end;

constructor TVisualSynapse.Create(TheOwner: TComponent;
  BackendSynapse: TSynapse);
begin
  inherited Create(TheOwner);
  if TheOwner is TWinControl then
    Parent := TheOwner as TWinControl;
  Self.DecimalPlaces := 8;
  Self.Increment := 0.1;
  Self.MaxValue:=10000;
  Self.MinValue := -MaxValue;
  FSynapse := BackendSynapse;
  Value := FSynapse.Weight;
  FSynapse.OnChange := @SynapseChange;
end;

procedure TVisualSynapse.Change;
begin
  inherited Change;
  FSynapse.Weight := Value;
end;

{ TNetworkVisualiser }

constructor TNetworkVisualiser.Create(AOwner: TControl; ANet: TNetwork);
begin
  inherited Create(AOwner);
  FNet := ANet;
  Layout;
end;

destructor TNetworkVisualiser.Destroy;
begin
  FLayerList.Free;
end;

procedure TNetworkVisualiser.Layout;
var
  L: Integer;
  objL: TLayer; objVL: TVisualLayer;
begin
  // Clean up
  RemoveChildern;
  FLayerList.Free;

  // Create the visible representation of network
  FLayerList := TVisualLayerList.Create(True); { the list will be the owner
    of the layer objects }
  for L := 0 to FNet.LayerCount - 1 do
  begin
    objL := FNet.Layers[L];
    objVL := TVisualLayer.Create(Self, objL); { the list doesn't own the
      shapes; the visualiser is the owner }
    FLayerList.Add(objVL);
  end;
end;

procedure TNetworkVisualiser.RemoveChildern;
var
  C: Integer;
  Child: TControl;
begin
  for C := ControlCount - 1 downto 0 do
  begin
    Child := Controls[C];
    RemoveControl(Child);
  end;
end;

{ TFrmNetworkTest }

procedure TFrmNetworkTest.FormCreate(Sender: TObject);
begin
  TestNet := TNetwork.Create([2, 2, 1]);
  TestNet.LearningRate := 0.1;
  TestVis := TNetworkVisualiser.Create(Self, TestNet);
  TestVis.Parent := Self;
  TestVis.Align := alClient;
end;

procedure TFrmNetworkTest.btEpochClick(Sender: TObject);
begin
  Epoch;
end;

procedure TFrmNetworkTest.btEvaluateClick(Sender: TObject);
begin
  Evaluate;
end;

procedure TFrmNetworkTest.btInputClick(Sender: TObject);
begin
  FeedInput;
end;

procedure TFrmNetworkTest.FormDestroy(Sender: TObject);
begin
  TestNet.Free;
end;

procedure TFrmNetworkTest.tbTrainChange(Sender: TObject);
var
  I: Integer;
begin
  if tbTrain.Checked then
  begin
    pbProgress.Position := 0;
    pbProgress.Step := 1;
    for I := 0 to pbProgress.Max - 1 do
    begin
      mmLog.Lines.Append(Format('Epoch %d:', [I + 1]));
      Epoch;
      {mmLog.Lines.Append(
        Format(
          'Epoch %04d: %1.8f',
          [I, TestNet.OutputLayer.Neurons[0].Error]
        )
      );}
      pbProgress.StepIt;
      Application.ProcessMessages;
      if not tbTrain.Checked then
        Break;
    end;
    tbTrain.Checked:=False;
  end;
end;

procedure TFrmNetworkTest.Epoch;
var
  N, P: Integer; // neuron, pattern
  I: Extended;
  InputPattern: array[0..1] of Extended;
  OutputPattern: array[0..0] of Extended;
  objN: TInputNeuron;
  objIL: TInputLayer;
  objOL: TOutputLayer;
  LogLine: String;
  PatternError: Extended;
  ErrorSum: Extended;
  AvgError: Extended;
  OutputValue: Extended;
begin
  LogLine := '';
  ErrorSum := 0;
  objIL := TestNet.InputLayer;
  objOL := TestNet.OutputLayer;
  for P := 0 to High(Inputs) do
  begin
    InputPattern := Inputs[P];
    OutputPattern := Outputs[P];
    // Feed the network with input
    for N := 1 to objIL.NeuronCount - 1 do
    begin
      objN := objIL[N];
      I := InputPattern[N - 1];
      objN.Input := I;
      Application.ProcessMessages;
    end;
    // Evaluate output
    TestNet.Evaluate(OutputPattern);
    OutputValue := objOL.Neurons[0].Value;
    if LogLine > '' then LogLine += ' ';
    LogLine += Format('%1.8f', [OutputValue]);
    PatternError := OutputPattern[0] - OutputValue;
    ErrorSum := ErrorSum + PatternError;
  end;
  AvgError := ErrorSum / Length(Inputs);
  LogLine += Format(' Error: %1.8f', [AvgError]);
  // Backpropagate average error
  //TestNet.Backpropagate([AvgError]);
  mmLog.Lines.Append(LogLine);
end;

procedure TFrmNetworkTest.FeedInput;
var
  N, P: Integer; // neuron, pattern
  I: Extended;
  InputPattern: array[0..1] of Extended;
  objN: TInputNeuron;
  objL: TInputLayer;
begin
  objL := TestNet.InputLayer;
  P := lbPatterns.ItemIndex;
  if P > -1 then
  begin
    InputPattern := Inputs[P];
    // Feed the network with input
    for N := 1 to objL.NeuronCount - 1 do
    begin
      objN := objL[N];
      I := InputPattern[N - 1];
      objN.Input := I;
      Application.ProcessMessages;
    end;
  end;
end;

procedure TFrmNetworkTest.Evaluate;
var
  P: Integer;
  OutputPattern: array[0..0] of Extended;
begin
  P := lbPatterns.ItemIndex;
  if P > -1 then
  begin
    OutputPattern := Outputs[P];
    // Evaluate output
    TestNet.Evaluate(OutputPattern);
  end;
end;

end.

