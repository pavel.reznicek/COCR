COCR
====

Cigydd's Optical Character Recognition - Teach the Computer to Read

This project is an attempt to create an optical character recognition (OCR) application 
concentrated on the ability to teach the underlying artificial intelligence
by correcting its mistakes. Many available OCR applications are trained once and forever
so they can't improve in the reading of the presented texts. COCR however aims to be 
a living and interactive system.

COCR uses an artificial neural network as the artificial intelligence core. Only CPU 
computation is supported at the moment.

The application is written in FreePascal/Lazarus IDE. It can be compiled on Linux, Windows,
and maybe other platforms too.

At the time of writing this, September 2014, COCR supports single character recognition only.
It's still a long way to go.

Happy teaching,

Cigydd
