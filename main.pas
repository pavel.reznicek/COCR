unit main;

{$mode delphi}{$H+}

interface

uses
  Classes, SysUtils, LResources, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  uSit, uNeuron, StdCtrls, Spin, Buttons, ComCtrls, Contnrs;

type

  { TfHlavni }

  TfHlavni = class(TForm)
    gbVstup: TGroupBox;
    gbVystup: TGroupBox;
    lIterace: TLabel;
    lVstupBin: TLabel;
    lVysledek: TLabel;
    lVystupBin: TLabel;
    panHlavniVeci: TPanel;
    pbIterace: TProgressBar;
    pbSada: TProgressBar;
    prTabulaRasa: TButton;
    chbVykreslovat: TCheckBox;
    gbAuto: TGroupBox;
    imgGraf: TImage;
    seIterace: TSpinEdit;
    seVstup: TSpinEdit;
    seVystup: TSpinEdit;
    tbUcit: TToggleBox;
    tbVysledek: TTrackBar;
    tlNahodnyVstup: TButton;
    tlOhodnot: TButton;
    tlSpatne: TButton;
    tlVstup: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure imgGrafClick(Sender: TObject);
    procedure KlepNaSit(Sender: TObject);
    procedure prTabulaRasaClick(Sender: TObject);
    procedure seVstupChange(Sender: TObject);
    procedure seVstupClick(Sender: TObject);
    procedure seVystupChange(Sender: TObject);
    procedure seVystupClick(Sender: TObject);
    procedure tbUcitChange(Sender: TObject);
    procedure tlNahodnyVstupClick(Sender: TObject);
    procedure tlOhodnotClick(Sender: TObject);
    procedure tlVstupClick(Sender: TObject);
  private
    { private declarations }
    Sit: TSit;
    Nezakazatelne: TComponentList;
  public
    { public declarations }
    procedure UkazVysledek;
    procedure KresliGraf;
    procedure Zpristupni(Pristupno: Boolean);
    procedure AtVstoupiVstup(TenVstup: Integer);
  end; 
  
function GetBitAsBool(Bit: Integer; C: Char): Boolean; overload;
{function GetBitAsBool(Bit: Integer; B: Byte): Boolean; overload;
function GetBitAsBool(Bit: Integer; W: Word): Boolean; overload;
function GetBitAsBool(Bit: Integer; L: Longword): Boolean; overload;}
function GetBitAsBool(Bit: Integer; Q: Qword): Boolean; overload;

function IntToBin(Value: QWord; Digits: Integer): String; overload;
function IntToBin(Value: QWord): String; overload;
function IntToBin(Value: Int64): String; overload;
function IntToBin(Value: Longword): String; overload;
function IntToBin(Value: Integer): String; overload;
function IntToBin(Value: Word): String; overload;
function IntToBin(Value: SmallInt): String; overload;
function IntToBin(Value: Byte): String; overload;
function IntToBin(Value: ShortInt): String; overload;
function CharToBin(Value: Char): String;

var
  fHlavni: TfHlavni;

implementation


function GetBitAsBool(Bit: Integer; C: Char): Boolean;
begin
  Result := (Ord(C) and (1 shl Bit)) > 0;
end;

{
function GetBitAsBool(Bit: Integer; B: Byte): Boolean;
begin
  Result := (B and (1 shl Bit)) > 0;
end;

function GetBitAsBool(Bit: Integer; W: Word): Boolean;
begin
  Result := (W and (1 shl Bit)) > 0;
end;

function GetBitAsBool(Bit: Integer; L: Longword): Boolean;
begin
  Result := (L and (1 shl Bit)) > 0;
end;
}

function GetBitAsBool(Bit: Integer; Q: Qword): Boolean;
begin
  Result := (Q and (1 shl Bit)) > 0;
end;

function IntToBin(Value: QWord; Digits: Integer): String;
var
  HexNum: String;
  I: Integer;
begin
  Result := StringOfChar('0', Digits);
  if Digits mod 4 = 0 then
    HexNum := IntToHex(Value, Digits div 4)
  else
    HexNum := IntToHex(Value, (Digits div 4) + 1);
  for I := 1 to Length(HexNum) do
  begin
    case LowerCase(HexNum[I]) of
      '0': Result := Result + '0000';
      '1': Result := Result + '0001';
      '2': Result := Result + '0010';
      '3': Result := Result + '0011';
      '4': Result := Result + '0100';
      '5': Result := Result + '0101';
      '6': Result := Result + '0110';
      '7': Result := Result + '0111';
      '8': Result := Result + '1000';
      '9': Result := Result + '1001';
      'a': Result := Result + '1010';
      'b': Result := Result + '1011';
      'c': Result := Result + '1100';
      'd': Result := Result + '1101';
      'e': Result := Result + '1110';
      'f': Result := Result + '1111';
    end;
  end;
  // Upravíme (ořízneme) na požadovanou délku
  Result := RightStr(Result, Digits);
end;

function IntToBin(Value: QWord): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: Int64): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: Longword): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: Integer): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: Word): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: SmallInt): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: Byte): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function IntToBin(Value: ShortInt): String;
begin
  Result := IntToBin(Value, SizeOf(Value) * 8);
end;

function CharToBin(Value: Char): String;
begin
  Result := IntToBin(Ord(Value), SizeOf(Value) * 8);
end;

{ TfHlavni }

procedure TfHlavni.FormCreate(Sender: TObject);
var
  N, S: Integer;
  objN: TNeuron;
begin
  Sit := TSit.Create(panHlavniVeci);
  Sit.Parent := panHlavniVeci;
  Sit.Align := alClient;
  for N := 0 to 8 do
  begin
    objN := Sit.PridejNeuron(0);
    objN.PridejVstup;
  end;
  Sit.Neuron[0, 8].PoleSynapsi[0].V := 1; // stabilisační vstup
  {
  for N := 0 to 7 do
  begin
    objN := Sit.PridejNeuron(1);
    for S := 0 to Sit.Vrstva[0].Count - 1 do
      objN.PridejSynapsi(Sit.Vrstva[0][S]);
  end;
  objN := Sit.PridejNeuron(1);
  objN.PridejVstup;
  objN.PoleSynapsi[0].V := 1; // stabilisační vstup
  }
  {
  for N := 0 to 7 do
  begin
    objN := Sit.PridejNeuron(2);
    for S := 0 to Sit.Vrstva[1].Count - 1 do
      objN.PridejSynapsi(Sit.Vrstva[1][S]);
  end;
  }
  objN := Sit.PridejNeuron(Sit.PocetVrstev, 150);
  for S := 0 to Sit.Vrstva[Sit.PocetVrstev - 2].Count - 1 do
    objN.PridejSynapsi(Sit.Vrstva[Sit.PocetVrstev - 2][S]);
    
  Sit.OnClick := KlepNaSit;
  Sit.DruhGrafu:=dgPlny;
  Nezakazatelne := TComponentList.Create(False);
  Nezakazatelne.Add(tbUcit);
  Nezakazatelne.Add(chbVykreslovat);
  Nezakazatelne.Add(gbVstup);
  Nezakazatelne.Add(gbVystup);
  Nezakazatelne.Add(gbAuto);
  Nezakazatelne.Add(tbVysledek);
end;

procedure TfHlavni.FormDestroy(Sender: TObject);
begin
  Nezakazatelne.Free;
end;

procedure TfHlavni.imgGrafClick(Sender: TObject);
begin
  KresliGraf;
end;

procedure TfHlavni.KlepNaSit(Sender: TObject);
begin
  ShowMessage(FloatToStr(Sit.VystupniVrstva[0].Hodnota));
end;

procedure TfHlavni.prTabulaRasaClick(Sender: TObject);
var
  PuvodniKresleni: Boolean;
begin
  Sit.FacTabulamRasam;
  UkazVysledek;
  KresliGraf;
  AtVstoupiVstup(seVstup.Value);
  PuvodniKresleni := Sit.Kreslit;
  Sit.Kreslit := True;
  Sit.Refresh;
  Sit.Kreslit := PuvodniKresleni;
end;

procedure TfHlavni.seVstupChange(Sender: TObject);
begin
  lVstupBin.Caption := IntToBin(Byte(seVstup.Value));
  AtVstoupiVstup(seVstup.Value);
  UkazVysledek;
end;

procedure TfHlavni.seVstupClick(Sender: TObject);
begin
  lVstupBin.Caption := IntToBin(Byte(seVstup.Value));
  AtVstoupiVstup(seVstup.Value);
  UkazVysledek;
end;

procedure TfHlavni.seVystupChange(Sender: TObject);
begin
  lVystupBin.Caption := IntToBin(Byte(seVystup.Value));
end;

procedure TfHlavni.seVystupClick(Sender: TObject);
begin

end;

procedure TfHlavni.tbUcitChange(Sender: TObject);
var
  {B,} I, J: Integer;
  //VstupBin: array[0..7] of Char;
begin
  if tbUcit.Checked then
  begin
    Zpristupni(False);
    pbIterace.Position := 0;
    pbIterace.Max := seIterace.Value;
    pbSada.Position := 0;
    pbSada.Min := seVstup.MinValue;
    pbSada.Max := seVstup.MaxValue;
    Sit.Kreslit := False;
    for I := 1 to seIterace.Value do
    begin
      pbSada.Position := pbSada.Min;
      for J := seVstup.MinValue to seVstup.MaxValue do
      begin
        //seVstup.Value := J;
        {
        for B := 0 to 7 do
        begin
          //VstupBin[7 - B] := Variant(Byte(GetBitAsBool(B, J)));
          varBit := Variant(GetBitAsBool(B, J));
          extBit := varBit; // pro kontrollu
          Sit.Neuron[0, 7 - B].PoleSynapsi[0].V := extBit;
        end;
        }
        AtVstoupiVstup(J);
        //lVstupBin.Caption := VstupBin;
        if J = seVystup.Value then
          Sit.Ohodnot([1])
        else
          Sit.Ohodnot([-1]);
        if Sit.Kreslit then Sit.Refresh;
        //UkazVysledek;
        pbSada.StepIt;
        Application.ProcessMessages;
        if not tbUcit.Checked then Break;
      end;
      pbIterace.StepIt;
      if chbVykreslovat.Checked then KresliGraf;
      Application.ProcessMessages;
      if not tbUcit.Checked then Break;
    end;
    tbUcit.Checked := False;
  end;
  Zpristupni(True);
  Sit.Kreslit := True;
  Sit.Refresh;
  UkazVysledek; { Pro formu, abychom měli lepší pocit, že se opravdu něco dělo,
    ukážeme výsledek posledního jednotlivého vstupu }
end;

procedure TfHlavni.tlNahodnyVstupClick(Sender: TObject);
begin
  seVstup.Value := Random(256);
  seVstupChange(seVstup);
  AtVstoupiVstup(seVstup.Value);
  Sit.Refresh;
  UkazVysledek;
end;

procedure TfHlavni.tlOhodnotClick(Sender: TObject);
begin
  AtVstoupiVstup(seVstup.Value);
  if seVystup.Value = seVstup.Value then
    Sit.Ohodnot([1])
  else
    Sit.Ohodnot([-1]);
  UkazVysledek;
  KresliGraf;
end;

procedure TfHlavni.tlVstupClick(Sender: TObject);
begin
  AtVstoupiVstup(seVstup.Value);
  Sit.Refresh;
  UkazVysledek;
end;

procedure TfHlavni.UkazVysledek;
var
  Vysl: Extended;
begin
  Vysl := Sit.Neuron[Sit.PocetVrstev - 1, 0].Hodnota;
  lVysledek.Caption := FloatToStr(Vysl);
  tbVysledek.Position := Trunc(Vysl * 1000);
end;

procedure TfHlavni.KresliGraf;
var
  I{, B}: Integer;
  H: Extended;
  Y1, Y2: Integer;
begin
  if imgGraf.Picture.Bitmap = nil then
    imgGraf.Picture.Bitmap := TBitmap.Create;
  if imgGraf.Picture.Bitmap.Width <> imgGraf.ClientWidth then
    imgGraf.Picture.Bitmap.Width := imgGraf.ClientWidth;
  if imgGraf.Picture.Bitmap.Height <> imgGraf.ClientHeight then
    imgGraf.Picture.Bitmap.Height := imgGraf.ClientHeight;
  imgGraf.Picture.Bitmap.Canvas.Brush.Color:=clWhite;
  imgGraf.Picture.Bitmap.Canvas.FillRect(imgGraf.ClientRect);
  for I := seVstup.MinValue to seVstup.MaxValue do
  begin
    {
    for B := 0 to 7 do
    begin
      Sit.Neuron[0, 7 - B].PoleSynapsi[0].V :=
        Variant(GetBitAsBool(B, I));
    end;
    }
    AtVstoupiVstup(I);
    H := Sit.Neuron[Sit.PocetVrstev - 1, 0].Hodnota;
    if Sit.DruhGrafu = dgPlny then
    begin
      if H >= 0 then
      begin
        Y1 := imgGraf.ClientHeight div 2 + Trunc((imgGraf.ClientHeight / 2) *
          -H);
        Y2 := imgGraf.ClientHeight div 2;
      end
      else
      begin
        Y1 := imgGraf.ClientHeight div 2;
        Y2 := imgGraf.ClientHeight div 2 + Trunc((imgGraf.ClientHeight / 2) *
          -H);
      end;
    end
    else
    begin
      Y1 := (imgGraf.ClientHeight - 1) div 2 + Trunc((imgGraf.ClientHeight / 2)
        * -H);
      Y2 := Y1 + 1;
    end;
    imgGraf.Picture.Bitmap.Canvas.Line(I, Y1, I, Y2);
  end;
  imgGraf.Refresh;
end;

procedure TfHlavni.Zpristupni(Pristupno: Boolean);
var
  I: Integer;
begin
  for I := 0 to Self.ComponentCount - 1 do
  begin
    if (Nezakazatelne.IndexOf(Components[I]) = -1)
    and (Components[I] is TControl) then
      (Components[I] as TControl).Enabled := Pristupno;
  end;
end;

procedure TfHlavni.AtVstoupiVstup(TenVstup: Integer);
var
  I: Integer;
  Bit: Extended;
begin
  for I := 0 to 7 do
  begin

    if GetBitAsBool(I, TenVstup) then
      Bit := 1
    else
      Bit := -1;

    //Bit := Variant(GetBitAsBool(I, TenVstup));
    Sit.Vrstva[0][7 - I].PoleSynapsi[0].V := Bit;
  end;
end;

initialization
  {$I main.lrs}

end.

