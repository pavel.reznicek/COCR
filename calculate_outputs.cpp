public void calculate_outputs()
{
    float f_net;
    int number_of_weights;
    for(int l=0;l<number_of_layers;l++)
        for(int i=0;i<layers[l];i++)
        {
            f_net=0.0F;
            if(l==0) number_of_weights=1;
            else number_of_weights=layers[l-1];             
            for(int j=0;j<number_of_weights;j++)
                if(l==0)
                    f_net=current_input[i];
                else
                    f_net=f_net+node_output[l-1,j]*weight[l,i,j];
            node_output[l,i]=sigmoid(f_net);
        }                 
}

