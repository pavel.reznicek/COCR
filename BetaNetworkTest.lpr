program BetaNetworkTest;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Classes,
  { you can add units after this }
  BetaNetwork, SysUtils;

var
  nn: TNeuralNetwork;
  i, j: Integer;
  training_sets: TTrainingSets;
  training_set: TTrainingSet;
  choice: Integer;
  training_inputs: TSingleArray;
  training_outputs: TSingleArray;
  //outputs_before_teaching: TSingleArray;
  outputs_after_teaching: TSingleArray;
  input_pattern: TSingleArray;
  output_pattern: TSingleArray;
  //Key: String;
  total_error: Single;
  total_error_below_minimum_epoch_counter: Integer;

const
  minimal_total_error = 0.001;
  max_count_of_epochs_with_total_error_below_minimum = 6;
begin
(*
###

*)

  // Blog post example:

  (*

  { nn = NeuralNetwork(2, 2, 2, hidden_layer_weights=[0.15, 0.2, 0.25, 0.3],
      hidden_layer_bias=0.35, output_layer_weights=[0.4, 0.45, 0.5, 0.55],
      output_layer_bias=0.6) }
  nn := TBetaNeuralNetwork.Create(2, 2, 2, [0.15, 0.2, 0.25, 0.3], 0.35,
    [0.4, 0.45, 0.5, 0.55], 0.6);
  // for i in range(10000):
  for i := 0 to 9999 do
  begin
    // nn.train([0.05, 0.1], [0.01, 0.99])
    nn.train(TSingleArray.Create(0.05, 0.1), TSingleArray.Create(0.01, 0.99));
    { print(i,
        round(nn.calculate_total_error([[[0.05, 0.1], [0.01, 0.99]]]), 9),
        nn.output_layer.get_outputs()) }
    WriteLn(i, ' ',
      FormatFloat(
        '0.000000000',
        nn.calculate_total_error(
          TTrainingSets.Create(
            CreateTrainingSet(
              TSingleArray.Create(0.05, 0.1),
              TSingleArray.Create(0.01, 0.99)
            )
          )
        )
      )
    );
  end;
  nn.Free;

  *)

  (*
  // XOR example:

  { training_sets = [
      [[0, 0], [0]],
      [[0, 1], [1]],
      [[1, 0], [1]],
      [[1, 1], [0]]
  ] }
  training_sets := TTrainingSets.Create(
    CreateTrainingSet(TSingleArray.Create(0, 0), TSingleArray.Create(0)),
    CreateTrainingSet(TSingleArray.create(0, 1), TSingleArray.Create(1)),
    CreateTrainingSet(TSingleArray.Create(1, 0), TSingleArray.Create(1)),
    CreateTrainingSet(TSingleArray.Create(1, 1), TSingleArray.Create(0))
  );

  // nn = NeuralNetwork(len(training_sets[0][0]), 5, len(training_sets[0][1]))
  nn := TBetaNeuralNetwork.Create(Length(training_sets[0][tsptInput]), 5,
    Length(training_sets[0][tsptOutput]), [], 0, [], 0);
  // for i in range(10000):
  for i := 0 to 9999 do
  begin
      // training_inputs, training_outputs = random.choice(training_sets)
      choice := Random(length(training_sets));
      training_inputs := training_sets[choice][tsptInput];
      training_outputs := training_sets[choice][tsptOutput];
      //nn.train(training_inputs, training_outputs)
      nn.train(training_inputs, training_outputs);
      // actual_outputs = nn.output_layer.get_outputs()
      outputs_after_teaching := nn.output_layer.get_outputs();
      { print("Iteration:", i,
          "Error:", nn.calculate_total_error(training_sets),
          "Inputs:", training_inputs, "Ideal outputs:", training_outputs,
          "Actual outputs:", actual_outputs) }
      WriteLn('Iteration: ', i,
        ' Error: ',
        FormatFloat(
          '0.000000000',
          nn.calculate_total_error(training_sets)
        ),
        ' Inputs: ', FormatSingleArray('0', training_inputs),
        ' Ideal outputs: ', FormatSingleArray('0', training_outputs),
        ' Current outputs: ', FormatSingleArray('0.000000000',outputs_after_teaching)
      );
      WriteLn('Press Enter to continue or type "q" and Enter to quit.');
      ReadLn(Key);
      if LowerCase(Key) = 'q' then break;
  end;
  *)

  // Negation example:

  {training_sets := TTrainingSets.Create(
    CreateTrainingSet(
      TSingleArray.Create(0, 1, 1, 1, 1, 1),
      TSingleArray.Create(1, 0, 0, 0, 0, 0)
    ),
    CreateTrainingSet(
      TSingleArray.create(1, 0, 1, 1, 1, 1),
      TSingleArray.Create(0, 1, 0, 0, 0, 0)
    ),
    CreateTrainingSet(
      TSingleArray.Create(1, 1, 0, 1, 1, 1),
      TSingleArray.Create(0, 0, 1, 0, 0, 0)
    ),
    CreateTrainingSet(
      TSingleArray.Create(1, 1, 1, 0, 1, 1),
      TSingleArray.Create(0, 0, 0, 1, 0, 0)
    )
  );}

  // Four different 12-member patterns with four outputs

  {SetLength(training_sets, 4);
  for i := 0 to High(training_sets) do
  begin
    SetLength(input_pattern, 12);
    for j := 0 to high(input_pattern) do
      if j = i then
        input_pattern[j] := 0
      else
        input_pattern[j] := 1;
    SetLength(output_pattern, 4);
    for j := 0 to high(output_pattern) do
      if j = i then
        output_pattern[j] := 1
      else
        output_pattern[j] := 0;
    training_set := CreateTrainingSet(input_pattern, output_pattern);
    training_sets[i] := training_set;
  end;}

  SetLength(training_sets, 4);
  for i := 0 to High(training_sets) do
  begin
    SetLength(input_pattern, 100);
    for j := 0 to high(input_pattern) do
      if j mod 4 = i then
        input_pattern[j] := 1
      else
        input_pattern[j] := 0;
    SetLength(output_pattern, 16);
    for j := 0 to high(output_pattern) do
      if (i and (1 shl j)) > 0 then
        output_pattern[j] := 1
      else
        output_pattern[j] := 0;
    training_set := CreateTrainingSet(input_pattern, output_pattern);
    training_sets[i] := training_set;
  end;


  // nn = NeuralNetwork(len(training_sets[0][0]), 5, len(training_sets[0][1]))
  nn := TNeuralNetwork.Create(Length(training_sets[0][tsptInput]),
    Length(training_sets), Length(training_sets[0][tsptOutput]), [], 1, [], 1);
  total_error_below_minimum_epoch_counter:=0;
  for i := 0 to 9999999 do
  begin
    choice := Random(length(training_sets));
    training_inputs := training_sets[choice][tsptInput];
    training_outputs := training_sets[choice][tsptOutput];
    //outputs_before_teaching := nn.feed_forward(training_inputs);
    nn.train(training_inputs, training_outputs);
    outputs_after_teaching := nn.output_layer.get_outputs();
    total_error := nn.calculate_total_error(training_sets);
    WriteLn('Iteration: ', i,
      ' Error: ',
      FormatFloat('0.000000000', total_error)//,
      //' Inputs: ', FormatSingleArray('0', training_inputs)
    );
    Writeln(
      '  Ideal outputs:           ',
      FormatSingleArray('0.000000000', training_outputs)
    );
    {WriteLn(
      '  Outputs before teaching: ',
      FormatSingleArray('0.000000000', outputs_before_teaching)
    );}
    WriteLn(
      '  Outputs after teaching:  ',
      FormatSingleArray('0.000000000', outputs_after_teaching)
    );
    {
    WriteLn('Press Enter to continue or type "q" and Enter to quit.');
    ReadLn(Key);
    if LowerCase(Key) = 'q' then break;
    }
    if total_error < minimal_total_error then // total error crossed minimum
      Inc(total_error_below_minimum_epoch_counter) // increment the counter
    else // set it to zero
      total_error_below_minimum_epoch_counter := 0;
    if total_error_below_minimum_epoch_counter =
      max_count_of_epochs_with_total_error_below_minimum then
    begin
      WriteLn(
        'The total error didn’t grow over the minimum in the last ',
        max_count_of_epochs_with_total_error_below_minimum,
        ' epochs. Ending the training process.');
      Break;
    end;
  end;

  // Finish
  WriteLn('Training done.');
  // Emit a sound
  Write(#7);
  Beep;
end.

