public void calculate_errors()
{
    float sum=0.0F;
    for(int n=0;n<number_of_output_nodes;n++)                   
        error[number_of_layers-1,n] = 
            (float)((desired_output[n]-node_output[number_of_layers-1,n])*
            sigmoid_derivative(node_output[number_of_layers-1,n]));

    for(int l=number_of_layers-2;l>=0;l--)
        for(int i=0;i<layers[l];i++)
        {
            sum=0.0F;
            for(int j=0;j<layers[l+1];j++)
                sum=sum+error[l+1,j]*weight[l+1,j,i];
            error[l,i] = (float)(sigmoid_derivative(node_output[l,i])*sum); 
        }
}


